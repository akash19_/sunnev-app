import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity, ImageBackground, Dimensions } from 'react-native';
import CountryFlag from "react-native-country-flag";
import { height, width } from 'react-native-dimension';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;
import StringsOfLanguages from '../Constants/StringsOfLanguages';


const CountrySelectscreen = ({navigation}) => {

  const settext = (value) => {
    StringsOfLanguages.setLanguage(value);
    navigation.navigate('Loginscreen', {selectedLanguage: value});
  };

  return (
    <View style={styles.container}>
      <View style={styles.logoCard}>
        <ImageBackground resizeMode='contain' source = {require('../Assets/icons/IB1.png')} 
          style = {styles.imageBackground}>
          <Image resizeMode='contain' source = {require('../Assets/icons/Applogo.png')} style = {styles.logo}/>
        </ImageBackground>
      </View>      
      <Image source = {require('../Assets/icons/name.png')} resizeMode='contain'
        style = {{ width: 140, height: 70, alignSelf: 'center', marginTop: 10, marginBottom: 30 }}/> 

        
      <View style={styles.countryBox}>
        <View style={styles.country}>
          <TouchableOpacity style={styles.countryCard} onPress={() => settext('en')} >
            <CountryFlag isoCode="um" size={25} style={[styles.flag]}/>
            <Text style={styles.flagName}>English</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.countryCard} onPress={() => settext('sp')}>
            <CountryFlag isoCode="es" size={25} style={[styles.flag]}/>
            <Text style={styles.flagName}>Español</Text>
          </TouchableOpacity>
        </View>     
      </View>        
    </View>
  )
}
export default CountrySelectscreen;

const styles = StyleSheet.create({  
  container: {  
    backgroundColor: 'white',
    flex: 1, 
  },
  logoCard: {
    width: '80%',
    height: windowHeight/2,
    margin: 30,
    marginBottom: 10,
    alignContent: 'center',
  },
  imageBackground: {
    width: width(80), 
    height: windowHeight/2, 
    alignSelf: 'center', 
    justifyContent: 'center',
  },
  logo: {
    width: 125, 
    height: 125, 
    alignSelf: 'center',
    marginTop: 9,
    marginLeft: 9,
    marginRight: 5,
  },
  countryBox: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', 
    bottom: 0,
  },
  country: {
    flex: 2,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  countryCard: {
    margin: 25,
  },
  flag: {
    width: 75,
    height: 75,
    borderRadius: 100/2,
  },
  flagName: {
    fontSize: 20,
    color: 'black',
    textAlign: 'center',
  },  
});  