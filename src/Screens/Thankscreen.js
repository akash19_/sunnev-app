import React from 'react';
import { Text, View, StyleSheet, Image, ImageBackground ,SafeAreaView} from 'react-native';
import {ProgressBar} from '@react-native-community/progress-bar-android';
import StringsOfLanguages from '../Constants/StringsOfLanguages';


const Thankscreen = ({route, navigation}) => {
  setTimeout(() => {
    navigation.navigate('Cusinescreen')
 }, 1000); 
  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        style={{flex: 1}}  source={require('../Assets/icons/welcomebg.png')} >
          <View style={{marginTop:90}}>
          {/* <ProgressBar styleAttr="Horizontal" /> */}
      <Image source={require('../Assets/icons/Welcomeicon.png')} resizeMode='contain'
        style = {{ margin: 30, width: 100, height: 100, alignSelf: 'center' }}/>        
      <Text style={styles.welcome}>{StringsOfLanguages.eight}</Text>
      <Text style={styles.content}>{StringsOfLanguages.nine}</Text>
      <Text style={styles.content}>{StringsOfLanguages.nine1}</Text>
      <Text style={styles.content}>{StringsOfLanguages.nine2}</Text>
      <Text style={styles.content}>{StringsOfLanguages.nine3}</Text>
      <Text style={styles.content}>{StringsOfLanguages.nine4}</Text>
    </View>
      </ImageBackground>
    </SafeAreaView>
  
  )
}
export default Thankscreen;

const styles = StyleSheet.create({  
  welcome: {
    textAlign: 'center',
    fontSize: 30,
    color: '#FF4433',
    marginBottom: 20,marginTop:15
  },
  content: {
    textAlign: 'center',
    fontSize: 25,
    color: 'black',
    paddingLeft: 45,
    paddingRight: 45,
  },
  container: {
    flex: 1,
    padding: 10,
  }
});  
