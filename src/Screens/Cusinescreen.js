import React, { useEffect, useState } from 'react';
import { Text, View,Image,FlatList, ScrollView,TouchableOpacity ,StyleSheet,ToastAndroid} from 'react-native';
import { Appbar } from 'react-native-paper';
import StringsOfLanguages from '../Constants/StringsOfLanguages';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';


const Cusinescreen = ({route, navigation}) => {

  const [selectedCusine, setSelectedCusine] = React.useState([]);  
  // const renderCusineSelect = ({ item, index }) => {
  //   const { id, title } = item;
  //   const isSelected = selectedCusine.filter((i) => i === title).length > 0; 
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');


 

  useEffect(() => {
    AsyncStorage.getItem('authToken').then(
      (value) =>{
        setAuthToken(value)

        AsyncStorage.getItem('UserId').then(
          (value1) =>{
            setUserID(value1)
            axios(`http://18.139.75.180:3000/sunev/api/v1/user/get_cusines/${value1}`,{
          "headers": {
            'Authorization':value
          }})
          .then((response) => setData(response.data))
          .catch((error) => console.error(error))
          .finally(() => setLoading(false));
      } 
    );
          }
          
        );

        
   
  }, []);
  
  const renderCusineSelect = ({ item, index }) => {
    const { cusine_id, title } = item;
    const isSelected = selectedCusine.filter((i) => i.cusine_id === cusine_id).length > 0; 

    return (
      <View style={{marginStart:15,marginEnd:15}}>          
        <TouchableOpacity onPress={() => {
          if (isSelected) {
            setSelectedCusine((prev) => prev.filter((i) => i.cusine_id !== cusine_id));
          } else {
            setSelectedCusine(prev => [...prev,{cusine_id: cusine_id}])
          } 
        }} 
        style={{margin:15, flex:2, flexDirection: 'row', marginBottom:0}}>
          <Image style={{width:30,height:30,alignSelf:'center',borderRadius:5}}

         source={isSelected
        ? require('../Assets/icons/button-clicked.png')  
        :require('../Assets/icons/button-not-clicked.png')} />

      <Image   source={{uri:'http://18.139.75.180/'+item.image}}
        style={{width:50,height:50,backgroundColor:'grey',alignSelf:'center',borderRadius:5,marginStart:15}} />
      <Text style={{fontSize:24,color:'black',alignSelf:'center',fontWeight:'normal',paddingLeft:20}}>{item.title}</Text>
    </TouchableOpacity>
      </View>
     
    );
  };


  return (
    <View style={{backgroundColor:'white'}}>
      <Appbar.Header style={styles.toolButton}>
        <Appbar.Action size={45} icon="chevron-left"   onPress={() => navigation.navigate('Thankscreen')} />
        <Appbar.Content  title="" subtitle="" />
        <Appbar.Action size={40} icon="menu" />
      </Appbar.Header>
      {/* <ProgressBar styleAttr="Horizontal" /> */}
      <View >
        <Text style={{fontSize:28,color:'black',alignSelf:'center',width:250,marginTop: 20, textAlign:'center'}}>{StringsOfLanguages.ten}</Text>
        <Text style={{fontSize:28,color:'black',alignSelf:'center',width:250, textAlign:'center'}}>{StringsOfLanguages.ten1}</Text>
      </View>      
      <FlatList 
        data={data.data}
        scrollEnabled={true}
        renderItem={renderCusineSelect} />
      <TouchableOpacity style={{backgroundColor:'#e7973d',width:'90%',height:50,alignSelf: 'center',
        marginBottom:0,margin:15, borderRadius: 10, justifyContent:'center'}} 
        onPress={() => {
          if(selectedCusine.length>0){
            axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_cusine',{
              "user_id" : userId,
              "preffered_cusine" :selectedCusine
            },
            {
             "headers": {
             'Authorization':authToken
            ,'Content-Type': 'application/json',
            }
            })
            .then((response) => {
              ToastAndroid.show(
                response.data.msg,
                ToastAndroid.SHORT
              );
              // console.log(selectedCusine)
              navigation.navigate('Proteinchoicescreen')
            })
            .catch((error) => {
             console.log("ERROR:::  " + error);
          }); 
          }else if(selectedCusine.length==0){
            alert('Please select prefered cusine')
          }
          
        }}>
        <Text style={{textAlign:'center',fontSize:22,color:'white'}}>{StringsOfLanguages.eleven1}</Text>
      </TouchableOpacity>
    </View>
  )
}

export default Cusinescreen;

const styles = StyleSheet.create({  
  container: {  
    backgroundColor: 'white',
    flex: 1, 
  },
  toolButton: {
    backgroundColor: 'white',  
    borderColor: 'white',
  }
});  