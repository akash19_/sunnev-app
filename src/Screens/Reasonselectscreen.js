import React, { useEffect, useState } from 'react';
import { Text, View,Image,FlatList,TouchableOpacity,StyleSheet,ToastAndroid } from 'react-native';
import StringsOfLanguages from '../Constants/StringsOfLanguages';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const ReasonSelectscreen = ({route, navigation}) => {
  const [selectedReason, setSelectedReason] = React.useState([]);

  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');
  

  useEffect(() => {
    AsyncStorage.getItem('authToken').then(
      (value) =>{
      setAuthToken(value);
      AsyncStorage.getItem('UserId').then(
        (value1) =>{
          setUserID(value1)
          axios(`http://18.139.75.180:3000/sunev/api/v1/user/visit_reasons/${value1}`,{
            "headers": {
            'Authorization':value
           }})
            .then((response) => setData(response.data))
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
          }
        );
        }
      );
  }, []);


  const renderReasonsSelect = ({ item, index }) => {
    const {reason_id, reason } = item;
    const isSelected = selectedReason.filter((i) => i.reason_id ===  reason_id).length > 0;

    return (
     <TouchableOpacity
        onPress={() => {
          if (isSelected) {
            setSelectedReason((prev) => prev.filter((i) => i.reason_id !== reason_id));
          } else {
            setSelectedReason(prev => [...prev, {reason_id: reason_id}])
          }
        }}>
          
          <View style={[styles.maincontainer,isSelected && {backgroundColor:'gray'}]} >
       <Text style={{fontSize:20,color:'white',alignSelf:'center',fontWeight:'bold'}}>{item.reason}</Text>
     </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{flex: 1}}>
        <Image source={require('../Assets/icons/Welcomeicon.png')} resizeMode='contain' style={styles.imgLogo} />
      <Text style={styles.textSty}> {StringsOfLanguages.fourth}</Text>
      <Text style={[styles.textSty,{marginBottom:10}]}>{StringsOfLanguages.five}</Text>
      <FlatList 
          data={data.data}
          scrollEnabled={true}
          renderItem={renderReasonsSelect}/>

      <TouchableOpacity style={styles.nextbtn}   onPress={() => {
          if(selectedReason.length>0){
            axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_visit_reason',{
              "user_id" : userId,
              "visit_reason" :selectedReason
            },
            {
             "headers": {
             'Authorization':authToken
            ,'Content-Type': 'application/json',
            }
            })
            .then((response) => {
              ToastAndroid.show(
                response.data.msg,
                ToastAndroid.SHORT
              );
              // console.log(selectedReason)
              navigation.navigate('Mainscreen')
            })
            .catch((error) => {
             console.log("ERROR:::  " + error.message);
          }); 
          }else if(selectedReason==0){
            alert('Please select reasons for visit')
          }
          
        }}
        >
    <Text style={styles.txtNext}>{StringsOfLanguages.eleven1}</Text>
  </TouchableOpacity>
    </View>
  )
}
export default ReasonSelectscreen;

const styles = StyleSheet.create({
  maincontainer:{
    justifyContent: 'center',
    backgroundColor: '#dd722e', 
    borderColor:'white',
    borderRadius:10,
    margin:4,
    height: 50, marginBottom: 8,
    marginStart:20,marginEnd:20,
    borderWidth:0.5
  },
  imgLogo: {
    width:90,height:90,
    marginTop:30,alignSelf:'center'
 },textSty:{
  fontSize:26,
  color:'black',
  alignSelf:'center',
  marginTop:0,
 },nextbtn:{
  padding: 10, 
  backgroundColor: '#e7973d',
  borderColor:'white',
  borderRadius:10,
  marginStart:20,marginEnd:20,
  marginBottom:30, borderWidth:0.5 
 },txtNext:{
  fontSize:20,color:'white',
  alignSelf:'center',paddingTop:3,
  fontWeight:'bold',marginBottom:10
 }
 });
 
