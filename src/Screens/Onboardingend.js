import React from 'react';
import { Text, View, StyleSheet, Image, TouchableOpacity } from 'react-native';
import { width, height } from 'react-native-dimension';
import { Appbar } from 'react-native-paper';
import StringsOfLanguages from '../Constants/StringsOfLanguages';
import AsyncStorage from '@react-native-community/async-storage';

const Onboardingend = ({route, navigation}) => {

  AsyncStorage.setItem('onBoardingEnd', "1");

  return (      
    <View style={styles.container}>
      <Appbar.Header style={styles.toolButton}>
        <Appbar.Action icon="chevron-left"   onPress={() => navigation.navigate('Proteinchoicescreen')} />
        <Appbar.Content title="" subtitle="" />
        <Appbar.Action icon="menu" />
      </Appbar.Header>

      <Image source = {require('../Assets/icons/Welcomeicon.png')} resizeMode='contain'
    
        style = {styles.logo}/>        
      <Text style={styles.thanks}>{StringsOfLanguages.thirteen}</Text>
      <View style={{marginBottom: 30}}>
        <Text style={styles.content}>{StringsOfLanguages.Fourteen}</Text>
        <Text style={styles.content}>{StringsOfLanguages.Fifteen}</Text>
        <Text style={styles.content}>{StringsOfLanguages.Sixteen}</Text>
        <Text style={styles.content}>{StringsOfLanguages.Seventeen}</Text>
        <Text style={styles.content}>{StringsOfLanguages.Eighteen}</Text>
      </View>
      <View style={styles.selectBox}>
        <TouchableOpacity style={[styles.button,{backgroundColor: 'white'}]} onPress={() => navigation.navigate('Dashboardpage')}>
          <Text style={styles.text2}>{StringsOfLanguages.Nineteen}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.button,{backgroundColor: '#ff9933'}]} onPress={() => navigation.navigate('Surveyscreen')}>
          <Text style={[styles.text2, {color: 'white'}]}>{StringsOfLanguages.Twenty}</Text>
        </TouchableOpacity>
      </View>      
    </View>
  )
}
export default Onboardingend;

const styles = StyleSheet.create({  
  container: {  
    backgroundColor: 'white',
    flex: 1, 
  },
  toolButton: {
    backgroundColor: 'white',  
    borderColor: 'white',
  },
  logo: { 
    margin: 25, 
    marginTop: 75,
    width: 100, 
    height: 100, 
    alignSelf: 'center', 
  },
  thanks: {
    textAlign: 'center',
    fontSize: 30,
    color: 'black',
    marginBottom: 25,
  },
  content: {
    textAlign: 'center',
    fontSize: 25,
    color: '#097969',
    paddingLeft: 20,
    paddingRight: 20,
  },
  selectBox: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', 
    marginTop: 5,
    bottom: 0,
  },
  button:{    
    width: width(90),
    height: height(8),
    alignSelf: 'center',
    borderStyle: 'solid', 
    borderColor: '#ff9933',
    borderWidth: 1,
    borderRadius: 10,
    justifyContent: 'center',
    marginBottom: 15
  },
  text2:{
    fontSize: 22,
    color: '#ff9933',
    textAlign: 'center',
    fontWeight: '500',
    paddingLeft: 25,
    paddingRight: 25,fontWeight:'bold'
  },
});  