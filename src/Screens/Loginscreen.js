import React, { Component ,useEffect, useState } from 'react';
import {
    Alert,
    StyleSheet,
    View,LogBox
} from 'react-native';
import Auth0 from 'react-native-auth0';
import axios from 'axios';

var credentials = require('../Constants/auth0-configuration');
const auth0 = new Auth0(credentials);
LogBox.ignoreLogs(["EventEmitter.removeListener"]);
import AsyncStorage from '@react-native-community/async-storage';



class Loginscreen extends Component {
    constructor(props) {
        super(props);
        this.state = { accessToken: null };
    }

    componentDidMount(){
        AsyncStorage.getItem('onBoardingEnd').then(
            (value) =>{
                if(value==1){
                    this.onLogout()
                  }else if(value==null){
                    this.onLogin()
                  }
            }
          );
    }

    onLogin = () => {
        auth0.webAuth
            .authorize({
                scope: 'openid profile email'
            })
            .then(credentials => {
                const AuthStr = 'Bearer '.concat(credentials.idToken);
                AsyncStorage.setItem('authToken', AuthStr);
                axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_user',{},
                {
                 "headers": {
                 'Authorization':AuthStr
                ,'Content-Type': 'application/json',
                }
                })
                .then((response) => {
                var userData=response.data.data
                this.props.navigation.navigate('Welcomescreen')
                this.setState({ accessToken: credentials.accessToken });
                AsyncStorage.setItem('UserId', response.data.data.user_id);
                })
                .catch((error) => {
                 console.log("ERROR:::  " + error.message);
              }); 
            })
            .catch(error => console.log(error));
    };

    onLogout = () => {
        auth0.webAuth
            .clearSession({})
            .then(success => {
                this.setState({ accessToken: null });
            })
            .catch(error => {
                console.log('Log out cancelled');
            });
    };

    render() {
        return (  
        <View style = { styles.container }>
                   
        </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF'
    },
    header: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10
    }
});

export default Loginscreen;