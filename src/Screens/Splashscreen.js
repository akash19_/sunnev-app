import React, {  useState } from 'react';
import {StyleSheet,ActivityIndicator,View,Image,SafeAreaView} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

const Splashscreen = (props) => {
const [onBoardingEndFlag, setonBoardingEndFlag] = useState('');

AsyncStorage.getItem('onBoardingEnd').then(
  (value) =>
  setonBoardingEndFlag(value)
);

setTimeout(() => {
  if(onBoardingEndFlag==1){
    props.navigation.navigate('Surveyscreen')
  }else if(onBoardingEndFlag==null){
    props.navigation.navigate('CountrySelectscreen')
  }
}, 2000);

  
return (
  <SafeAreaView style={styles.maincontainer}>
   <Image resizeMode='contain' style={styles.imgLogo} source={require('../Assets/icons/AppIcon.jpg')} />
   {/* <ActivityIndicator style={styles.container} size="large" color="#dd722e" /> */}
  </SafeAreaView>
);
};

const styles = StyleSheet.create({
 maincontainer:{
  backgroundColor:'white',
  alignItems:'center',
  flex:1,
  justifyContent:'center'
 },
 imgLogo: {
  width: 150,
  height: 253
}
});

export default Splashscreen;
