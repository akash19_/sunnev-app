import React, { useState,useEffect,useReducer } from "react";
import { Text, View,StyleSheet, FlatList,TextInput,SafeAreaView, Image, TouchableOpacity ,ScrollView,LogBox,BackHandler,Alert,ToastAndroid} from 'react-native';
import { useNavigation } from '@react-navigation/native';
import StringsOfLanguages from '../../Constants/StringsOfLanguages';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
let demographyAnswer={};

const DATA1 = [
  {
    id: '1',
    title:  [StringsOfLanguages.Thirtytwo],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAAArCAYAAADVJLDcAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAwdJREFUeNrsmUloFEEUhmva0RhBXIi4JBKjQYUgoqAHCSoeghuCioImOQTcSEQQ3HIQ5iDqQc+KevDgkuAyBxfEiyDmImgkiAkx7jAYjQZRDwk68X/0E0NTvVRVm+ke5sF3qemqfn/X8uq9SWxLnxMhWQLsAwdAmcszQ+A1OAquipAtGeJY+8HpAILngCtgENz4H2LGgD1gM5gBLJ9+r0CNo22H4rsbJGK6waiA/bMgw2OcpY9DYorBPbBcwZFBSdtkRTFTJG1l7E9Qq2S/aRJW0wwcURRCNk3S9khxjIeSj1GsucLI/2YSU6/ReSJY5Gg7DN4H7N8BTkgcMrE6Wmblmp1rQbtjH1WBdTxmQtLnN+gBdyVLtc5QTHkywGZ3syZwAXQNa/sBWjXGWgU2GYqxLIPOY8EtUGroBM1mi8tMqqkx7E+nyWOwVvPdtF/bXE62nARNikt3wFNwEzwDAx7PjwNL+TidH9UbwGImZ2aJPLKCmHwScx2sAFNBBdgFPsRRzDGwhe9Vn8BbcJ5PpzdxEtMJUi6/feR8JqemcjS38t3Kzeg20A8meTxDoveCb4Z+U86zG2zUFfM8QLL0kpecmz0IMbsccIqxQl6Sfs8MhbiqfpnsmSU+vxeBeQGWR1hWZLJnGjih6nf5fScY7zMGXUibwZcQ9nq9iZgSjjFrJIlVdYDKjGCxx6MSZyiJOiTJa1q4whO7G0C1JKcpFREw1RSA4sxFR9sLcBusDzjGZ/Bd09/RYKapGDrTL4FTjpz/b3zZALaDg2ChxzhtXIXJGkzASWFXgrSW2TUwW9gVyy6P+HFZ2OUnurtlXJ57ZyiErEN3zzSCrR7OyUTRibcAPInSAZAGZzTH/crLLiupF5hWYebq7Jn7hi/t5mVVMaxtpbD/0tANmnT8V+mImR7CzMtizyxmRJdZo+OrqlrtSMefpM/1pZ1jyE/FcanWXBO1oDmBv3ChOlMQUxDzT0wmT7RkLI70+WBpEpPiqkqcjfxPkZg+sEzY/6X3xkxEL/tN/vf9EWAA3V6E+0KH/IMAAAAASUVORK5CYII=',
    link: 'Demographeyscreen',
  }, {
    id: '2',
    title:  [StringsOfLanguages.Thirtythree],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAzBAMAAAAnTUYnAAAAMFBMVEVHcExZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5q6f5SAAAAAD3RSTlMAefEtuQYPoM1B4WgcU4nsArSrAAAB1klEQVQ4y82SvS9DURjGD1fro0oTISFBQmJB2sEgDBpfg+UaJAwGkRSJhUQwSEoMBkNvExYGAwaWlsFg0aE138nKYpZeTdpc0df5vqecP8A73LzP/Z373uc85yBEawnpKzgOB1rmNwHgUkfmMAAnoiHdhMC+kCe3YrQvTMk3B6u4v2FtPQVQ5MT0RgcYKXCfVEQ1pIaKT820aiq+NA6aFVLpukFdtqDutImSPiUdvgXUSGeHRKKmkuir55NUzGuNBDgh/ZkYdy+yH92a3DzVrPE9Um974rTOuzq5zzOWAbSyRIhRt420VRxAgdjzx2nv2jIBojLrOw8GiwryMk9cvcEwuNYFVzaqk+SNvPys5WoAjUkSS+FHyQAx7kmAEpsbSXGJPgQpMpO2yaTjWSuy/NN/SYHdByvBtSBHu84iXbqCW5VMIDRkk1/mAzi/YY8UUbIdWeR+b9SjZIvPlOR6HmAa+TrgGKEs3vOyJGk8yLXo0eKI4KtKkhzJsTwrsnYNSagtOMRkhHaNksTY5mZ6rljjfWPHQa1yQJLoWgWZWpAk71c/Ks9+eBm8B8MSuNa8kk4pk5Vk2kioiQ7K0PvRfUXWsC2uSiRbeQreBcilfhFHkGf4L+QHswAVmC2cXd8AAAAASUVORK5CYII=',
    link: 'FamilyScreen',
  }, {
    id: '3',
    title:  [StringsOfLanguages.ThirtyFour],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADIAAAAxCAMAAACvdQotAAAAM1BMVEVHcExZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5qRyy7YAAAAEHRSTlMAEHy59h5rienUB1ieLEk8uv+h8AAAAXZJREFUSMedVtm2wiAMLFvZWuD/v/b2XIWSjap5kzGQZGai29YjFe317jYSKl9AVvTc2/Yf5oSAO97AgW47Yxuxz0AN49xmcJVtU/gpAwDTZSk2EGWUG8C5vYveYUaLHfAIMCMlIKQ/oywG+ghODPRuCgF6N5kghi/4mvRTihZTyvev/NDLFjFS33yJE/uFF8x+fWZ/cwDSksayoGQrKrlA9ScdeL90I9mDui+V/TIf58rCu/KrULunoefyUoafHVE+duBrAsed4vmMFhK687a3EVKGNvqdVj2+0hsJpNRTSPHETEN5VMavOGnhooyhZhWnPbec8c55gp3ZGE9kTZGZFN7jg05q13vFHjy96JwnBS3r8gEpuLIgicVIUxEXCZ3/cH8VSNEiw1g0PClINJptsjKECaKp4vAl0QRxkLPCgfr0QkiTn+dfRbe0eGAqM0tTsHLOIimSaFakoMo8bu85Rb2+EtV6J4B/OElHG/204DXHS/DpDwkwWodEycx7AAAAAElFTkSuQmCC',
    link: 'IdentityScreen',
  }
]

const reducer = (state, action,index) => {
  console.log('sssss ',index)
  switch (action.type) {
    case 'increment':
      return { ...state, count: state.count + action.payload };
    case 'decrement':
      return { ...state, count: state.count - action.payload };
    default:
      return state;
  }
};


const Demography = () => {

  const [state, dispatch] = useReducer(reducer, { count: 0 });
 
  const [isVisible, setIsVisible] = useState(false);
  const [selectedData, setSelectedData] = React.useState([]);
  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');  
  const [FamilInfo, setFamilInfo] = useState([]);
  const [FamilInfoTitle, setFamilInfoTitle] = useState('');
  const [EthnicityData, setEthnicityData] = useState([]);
  const [EthnicityTitle, setEthnicityTitle] = useState('');

  const [FamilyUnitInfoDataa, setFamilyUnitInfoData] = useState([]);
  const [FamilyUnitInfoTitle, setFamilyUnitInfoTitle] = useState('');

  const [identityTitle, setidentityTitle] = useState('');  
  const [identifyObejct, setidentityObejct] = useState([]);


  const [demographyFlag, setDemographyFlag] = useState(false);
  const [healthhistoryFlag, setHealthhistoryFlag] = useState(false);
  const [foodMoodFlag, setfoodMoodFlag] = useState(false);
  const [activityFlag, setActivityFlag] = useState(false);
  

  useEffect(() => {
    AsyncStorage.getItem('authToken').then(
      (value) =>{
      setAuthToken(value);
      AsyncStorage.getItem('UserId').then(
        (value1) =>{
          setUserID(value1)
          axios(`http://18.139.75.180:3000/sunev/api/v1/user/user_questionnaire/${value1}`,{
            "headers": {
            'Authorization':value
           }})
            .then((response) => {
              
              demographyAnswer=response.data.data.demography

              setDemographyFlag(response.data.data.is_demography_complete)
              setHealthhistoryFlag(response.data.data.is_healthhistory_complete)
              setfoodMoodFlag(response.data.data.is_foodmood_complete)
              setActivityFlag(response.data.data.is_activity_complete)
            
              
              for(let EthnicityDataList of response.data.data.demography.Personal_Information){
                if(EthnicityDataList.section_title=="Ethnicity"){
                  setEthnicityData(EthnicityDataList.questions)
                  setEthnicityTitle('Ethnicity')
                }
              }

              for(let identifyObejctList of response.data.data.demography.Identity){
                setidentityTitle(identifyObejctList.section_title)
                setidentityObejct(identifyObejctList.questions)
              }
              
  
              for (let FamilyUnitInfoData of response.data.data.demography.Family_Information) {
                  setFamilyUnitInfoTitle(FamilyUnitInfoData.section_title)
                  setFamilyUnitInfoData(FamilyUnitInfoData.questions)
                  break
                }
    
              
              for (let PersonalInf of response.data.data.demography.Personal_Information) {
                if(PersonalInf.section_title=="Let's fill in your personal information"){
                  setFamilInfo(PersonalInf.questions)
                  setFamilInfoTitle(PersonalInf.section_title)
                  break 
                }
                 
              }
    
            })
            .catch((error) => console.error(error))
          }
        );
        }
      );

     
    LogBox.ignoreLogs(["VirtualizedLists should never be nested"])
  }, [])

  useEffect(() => {
    const backAction = () => {
      
    };

    const backHandler = BackHandler.addEventListener(
      "hardwareBackPress",
      backAction
    );

    return () => backHandler.remove();
  }, []);

  const handleChangeText =(index,onChangeText)=>{
      demographyAnswer.Personal_Information[0].questions[index].value=onChangeText
  }

  


  const renderItemFamilyInfo = ({ item,index }) => (
    <View style={{marginTop:4}}>      
    <Text style={styles.itenText}>{item.title}</Text>
    <TextInput defaultValue={item.value} onEndEditing={e => handleChangeText(index,e.nativeEvent.text)} style={styles.input}  />
    </View> 
  );

  const renderItem1 = ({ item }) => (
    <TouchableOpacity style={{margin: 20, flexDirection: 'row',justifyContent:'space-between',paddingEnd:10, borderColor: '#59af9a', borderWidth: 2, borderRadius: 10, alignContent: 'space-around', marginBottom: 10, marginTop: 0
      }} onPress={() => setIsVisible(!isVisible)}>
      <Text style={{fontSize: 20, color: '#59af9a', alignSelf: 'flex-start', margin: 20,fontWeight:'bold'}}>{item.title}</Text>
      <Image source={{uri:item.photo}}
        style={{width: 25, height: 25,marginEnd:10, alignSelf:'center',justifyContent:'flex-end'}} />    
    </TouchableOpacity>
  );

 

  const renderItem12 = ({ item, index }) => {
    if(item.type=='radio'){
      return (
        <View   style={{width: '100%'}}    >
                 <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5,padding:5,fontSize:18}}>{item.title}</Text>
                    
  
                  <FlatList 
                      data={item.options}
                      scrollEnabled={true}
                      numColumns={7}
                      listKey={(item2, index) => 'E' + index.toString()}
                      columnWrapperStyle={styles.wrapView}       
                      style={{width: '95%', margin: 5, marginTop: 5}}
                      renderItem={(childData) => renderItemInsideOptionsRadio1(childData, index)} />
        </View>    
      );
    }
   
  };

  const renderItem3 = ({ item, index }) => {
    return (
      <View style={{width:'100%'}}>
                <FlatList 
                    data={item.options}
                    scrollEnabled={true}
                    numColumns={7}
                    listKey={(item, index) => 'D' + index.toString()}
                    columnWrapperStyle={styles.wrapView}       
                    style={{width: '95%', margin: 5, marginTop: 5}}
                    renderItem={(childData) => renderItemInsideOptions(childData, index)} />
      
      </View>    
    );
  };

  const renderItemInsideOptions = ({ item ,index},questionIndex) => {
    const { title, isSelected } = item;
    return (
      <TouchableOpacity
      onPress={() => {
        if (isSelected) {
          setSelectedData((prev) => prev.filter((i) => i !== title));
          handlleChangeUncheck(index,questionIndex) 
        } else {
          setSelectedData(prev => [...prev, title])
          handlleChange(index,questionIndex)
        }
      }}
      style={[
        {
          borderColor: '#59af9a', height: 35, borderWidth: 1.5,
           color: 'black', alignSelf: 'flex-start', borderRadius: 5,
            marginLeft: 5,
       marginRight: 0, marginBottom: 0, marginTop: 5,
        justifyContent: 'space-evenly',backgroundColor:'white'}, 
      item.isSelected && {  backgroundColor:item.isSelected? '#59af9a': 'white'}]}>
      <Text style={{fontSize: 15, textAlign: 'center', marginLeft: 15, marginRight: 15, color: isSelected ? "white" : "black"}}>{item.title}</Text>
    </TouchableOpacity>  
    );
  };


  const renderItemInsideOptionsRadio1 = ({ item,index },questionIndex) => {
    var { title, isSelected } = item;
    return (
      <TouchableOpacity 
      style={[{borderColor: '#59af9a',backgroundColor:'white', height: 35, borderWidth: 1.5, color: 'black', alignSelf: 'flex-start', borderRadius: 5, marginLeft: 5, marginRight: 0, marginBottom: 0, marginTop: 5, justifyContent: 'space-evenly'}, 
        { backgroundColor: isSelected ?'#59af9a':'white'}]}>     
       
      <Text style={{fontSize: 15, color: 'black', textAlign: 'center', marginLeft: 15, marginRight: 15}}>{item.title}</Text>
    </TouchableOpacity>    
    );
  };

  const renderItemOptionSelect = ({ item, index }) => {
    return (
      <View   style={{width: '100%'}}    >
      <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5}}>{item.title}</Text>
         <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
        
         </View>  

       <FlatList 
           data={item.options}
           scrollEnabled={true}
           numColumns={7}
           listKey={(item2, index) => 'E' + index.toString()}
           columnWrapperStyle={styles.wrapView}       
           style={{width: '95%', margin: 5, marginTop: 5}}
           renderItem={renderItemInsideOptionsRadio} />
        </View>      
    );
  };


  const renderItemInsideOptionsRadio = ({ item }) => {
    return (
      <TouchableOpacity style={{borderColor: '#59af9a',backgroundColor:'white', height: 35, borderWidth: 1.5, color: 'black', alignSelf: 'flex-start', borderRadius: 5, marginLeft: 5, marginRight: 0, marginBottom: 0, marginTop: 5, justifyContent: 'space-evenly'}}>      
      <Text style={{fontSize: 15, color: 'black', textAlign: 'center', marginLeft: 15, marginRight: 15}}>{item.title}</Text>
    </TouchableOpacity>    
    );
  };

  const handlleChange = (index,questionIndex) => {
    demographyAnswer.Personal_Information[1].questions[questionIndex].options[index].isSelected=true   
  };

  const handlleChangeUncheck = (index,questionIndex) => {
    demographyAnswer.Personal_Information[1].questions[questionIndex].options[index].isSelected=false 
  };


  const renderItemFamilyUnitinfo = ({ item, index }) => {
    if(item.type=='number'){
      return (
        <View style={{ flex:2, justifyContent: 'center',  alignItems: 'center',borderRadius:10}} >
        <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', marginTop: 5}}>{item.title}</Text>
        <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: 'white', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
        <TouchableOpacity  onPress={() => {dispatch({ type: 'decrement', payload: 1,index:index });}}  >
        <Image source={require('../../Assets/icons/minus-icon-blue.png')} 
            style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
          </TouchableOpacity>

          
          <TextInput value={state.count.toString()} style={{flex:1, color: 'black'}} />
          <TouchableOpacity  onPress={() => {dispatch({ type: 'increment', payload: 1,index:index });}}  >
          <Image  source={require('../../Assets/icons/expand-plus-icon-green.png')}
            style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
          </TouchableOpacity>
          
        </View>  
      </View>        
      );
    }  
  };

  

  

  return (
    <View style={{flex: 1}}>
      {!isVisible ?
        (
          <View style={{flex: 1, marginBottom: 20}}>

            <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: demographyFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: healthhistoryFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: foodMoodFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: activityFlag ? '#59af9a' : 'white',marginStart:5}} />
            </View>
           

            <View style={{backgroundColor: '#D3D3D3', color: 'black', alignSelf: 'flex-start', padding: 10, borderRadius: 10, borderTopLeftRadius: 0, margin: 20, marginBottom: 0, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 18, color: 'black', textAlign: 'left', marginLeft: 5}}>{StringsOfLanguages.TwentyFive}</Text>
            </View>
            <View style={{backgroundColor: '#D3D3D3', width: '90%', color: 'black', alignSelf: 'flex-start', padding: 10, borderRadius: 10, borderTopLeftRadius: 0, margin: 20, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 18, color: 'black', textAlign: 'auto', marginLeft: 5}}>{StringsOfLanguages.TwentySix}</Text>
            </View>
            <FlatList 
              data={DATA1}
              scrollEnabled={true}
              renderItem={renderItem1} />
            <View style={{alignSelf: 'center', margin: 20, marginBottom: 5, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 20, color: '#7e7e7e', textAlign: 'center'}}>{StringsOfLanguages.TwentySeven}</Text>
            </View>
            <TouchableOpacity style={{borderColor: '#e7973d', width: '90%', height: 50, alignSelf: 'center', borderWidth: 2,
              marginBottom: 0, margin: 18, marginTop: 0, borderRadius: 10, justifyContent: 'center'}}  >
              <Text style={{textAlign: 'center', fontSize: 20, color: '#e7973d',fontWeight:'bold'}}>{StringsOfLanguages.Nineteen}</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      {isVisible ?
        (
          <ScrollView>
            <View style={{flex:1,marginTop:10,marginStart:15,marginEnd:15,marginBottom:8}}>
               <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            </View>
              <Text style={styles.text1}>{FamilInfoTitle}</Text>
              <SafeAreaView>
                <FlatList 
                  data={FamilInfo}
                  scrollEnabled={false} horizontal={false}
                  renderItem={renderItemFamilyInfo} 
                  
                  />
              </SafeAreaView>      
              <View>
                <Text style={{fontSize: 18, color: 'black', textAlign: 'left', margin: 15, marginTop: 5, marginBottom: 0}}>{EthnicityTitle}</Text>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, marginBottom: 0, margin: 10}}>
                  <TextInput style={{flex:1, color: 'black'}} />
                  <Image  source={require('../../Assets/icons/expand-plus-icon-green.png')}  
                    style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
                </View>
                <View style={{marginBottom: 0}}>

                  

                  <FlatList 
                    data={EthnicityData}
                    scrollEnabled={true}
                    columnWrapperStyle={styles.wrapView}
                    numColumns={7}
                    style={{width: '95%', margin: 10, marginTop: 5}}
                    renderItem={(childData) => renderItem3(childData)}
                     />

                </View>
                <Text style={{fontSize: 18, color: 'black', textAlign: 'left', margin: 15, marginTop: 0, marginBottom: 0}}>{FamilyUnitInfoTitle}</Text>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
                  <TextInput style={{flex:1, color: 'black'}} />
                  <Image  source={require('../../Assets/icons/expand-plus-icon-green.png')}  
                    style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
                </View>

                <FlatList
                    data={FamilyUnitInfoDataa} 
                    scrollEnabled={true}
                    numColumns={2}
                    renderItem={renderItemFamilyUnitinfo} />

                    <FlatList 
                    data={FamilyUnitInfoDataa}
                    scrollEnabled={false}
                    renderItem={renderItem12} />




                <Text style={{fontSize: 18, color: 'black', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 0}}>{identityTitle}</Text>
               
                <FlatList 
                  data={identifyObejct}
                  scrollEnabled={true}
                  numColumns={3}
                
                  renderItem={renderItemOptionSelect} />

                <TouchableOpacity onPress={()=> {
                   axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_demography',{
                    "user_id" : userId,
                    "demography" :demographyAnswer
                  },
                  {
                   "headers": {
                   'Authorization':authToken
                  ,'Content-Type': 'application/json',
                  }
                  })
                  .then((response) => {
                    ToastAndroid.show(
                      response.data.msg,
                      ToastAndroid.SHORT
                    );
                   
                  })
                  .catch((error) => {
                   console.log("ERROR:::  " + error.message);
                }); 
                }} style={{backgroundColor:'#59af9a',width:'95%',height:50,alignSelf: 'center',
                  marginBottom:15, margin:15, borderRadius: 10, justifyContent:'center'}} >
                  <Text style={{textAlign:'center',fontSize:22,color:'white'}}>{StringsOfLanguages.FourttyTwo}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        ) : null}
    </View>    
  )
}
export default Demography;

const styles = StyleSheet.create({
  text1:{
    alignSelf:'flex-start',
    fontSize:20,
    paddingLeft: 15, paddingRight: 15,
    color:'#000000',marginBottom:15
  }, 
  text2:{
    fontSize:20,
    color:'#000000',marginTop:10,marginStart:13
  },
  input: {
    height: 60,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:3,paddingStart:10,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  itenText:{
    fontSize:18,color:'#59af9a',
    fontWeight:'bold',paddingStart:15
  },
  inputEthnivity: {
    height: 120,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:5,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  wrapView: {
    flexWrap:'wrap',
  },
});