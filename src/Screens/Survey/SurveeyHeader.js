import React, {  useState } from 'react';
import { Text, View,StyleSheet ,Image, ImageBackground} from 'react-native';
import { Appbar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import AsyncStorage from '@react-native-community/async-storage';

const SurveeyHeader = (props) => {
  const navigation = useNavigation(); 

  const [onBoardingEndFlag, setonBoardingEndFlag] = useState('');

  AsyncStorage.getItem('onBoardingEnd').then(
    (value) =>
     setonBoardingEndFlag(value)
    );

  return (
    <View>
      <Appbar.Header style={styles.toolButton}>
        <Appbar.Action size={35} icon="chevron-left"   onPress={() => navigation.navigate('Onboardingend')} />

        {/* <Appbar.Action size={35} icon="chevron-left"   onPress={() => {if(onBoardingEndFlag==1){
                                    }else if(onBoardingEndFlag==null){
                                   navigation.navigate('Onboardingend')
                                     }}} /> */}

        <Appbar.Content title="" subtitle="" />
       
        <ImageBackground
          style={{width: 25, height: 25}}
          source={{uri:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACEBAMAAACjap6UAAAAIVBMVEVHcEx1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXUW5UrxAAAACnRSTlMAQNHssG8RV5QoA+O9TAAAAxtJREFUaN7tWEtv00AQTtLUTjgVOETKKbTi5RO0AlGfcqLgUwQCqpyiAuJxSlBBglMFFImcKopaxKmxHSj7KylkvV7bs/aMO0Ic8p2sXe+3u/PamalU8nD4ccUNVu+8rZSF/UFI3BqWY/jmCoXgcxmGryKBu3SG5yKFT2Q5uGmKgCgPyxMZhAMSxRcB4CqFoepCFMESgeK8AHH9tIcgHWNfGLCOVodjovCxSqlra3ZHrU2NsYukeKdWbPzd1dpWAz+R93AzfqH8JcDdpAHoUGl5B0XxHTBoZfC/UBQetGF0tBDD0JQ/HyeHx3J4jyCKLqxpjDAWYNlHerqGt4pJeryNt4yxwQ7roIhAuAaxSTEHaIWERmUXq8Q2XlkKaYjV6Y/szEusVqXUOtmZGtbfa8a9GkZy2LJ62ZlFrG0tGIVmYynOzH4EYnV1NnP0LygYLsIgTgalMpgWg4EzuBmDszOEHI7AxxB+GR4BhqeI4UFkeJY5kgOGFIUhUWJI1ziSRobUlSGB5kjjGYoJjpKGobDiKO8YikyOUpej4OYo+zmaDxwtkDn+G7xovX4A4NHoALe+ubkijFjdLX4QrW1X5CLYKPB52xOFCHOt/akrEAge5sQIFMMJhzF2LCIZTjh6hrDtCDR8MJhbY0HAMaSXc4KEC8ZSAY+eKWMlXCXN8ESQcTs/pVhbBrCWm2xs6XNXRgY3sFrv9f8uJSa1Q4Rv8lzgmWd4ZLXs6mKBI1r34MwrVsfl4oDyClKKrQZvYILSffX7MJue4VKIOPlYzwqzh4uNypL9zAg6HdtP7xml/v4elqLppAoDDzSVXGwly5MqOTvWjrGk1+KE7FiTRidRO5AS02qiunDwNWzWoH2dr0OjqGlnr5e5R7xzN25LEO+hbvKn0dGffd6kUjyerZvG0tyhUjSUPC15pwGVIl5oUwppqDAfRgqZ0CnakUpq+LYE3Ojo4FtN5vZVG9tqMrWvJpFZDOgUVmQY42QQpMCRZu2V1WlFLXXKeUjsJb5spk3LUPRlu43W2oBaJXwU07Ml0E9QnAZzijnFnCIfvwHnd+SGyMAN3wAAAABJRU5ErkJggg=='}} >
          <Image source={require('../../Assets/icons/reddot.png')} style={{width: 8, height: 8, alignSelf: 'flex-end', marginTop: -5, marginRight: -5, position: 'absolute'}} />
        </ImageBackground>
       <Appbar.Action size={35} icon="menu" />
      </Appbar.Header>
    </View>
  )
}
export default SurveeyHeader;


const styles = StyleSheet.create({  
  toolButton: {
    backgroundColor: 'white',  
    borderColor: 'white',
  }
});  