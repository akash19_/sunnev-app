import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, FlatList, TextInput, Image, TouchableOpacity, ScrollView, LogBox,ToastAndroid } from 'react-native';
import StringsOfLanguages from '../../Constants/StringsOfLanguages';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';
let activityAnswer={};

const DATA1 = [
  {
    id: '1',
    title: 'Physical Activities',
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD4AAABECAMAAADupnbRAAACQ1BMVEUAAAAA//+AgIBVqqpAv4BmmZlJtpJgn59Vqo5Ns5lduaJVqpVbtpJVqplgr59atJZVqpxerpRZs5ldrpdZsZtVqpVcrZlbrZtYsJ5Vs5larZxYr5ddsptarZ5XrppZrJlXrptZsppXrpxbsJlZsZtar5pXrZlar5tYsZhXrZpar5xYsJlZrphbrZxZrplYsJtasZhasZlZrptYr5hasJpZrptXr5lZrphasJtYrZlbrppZsJhYsZparptarptZr5pYsJtarppZr5tarppZr5tasJtZrplar5pZsJtYrppZsJlYrppYrppZsJpYrptar5lZr5pYrplar5pZrppZr5lYsJpZrptZrplZr5pZrplYr5pZrppYr5tar5lYr5tar5pZsJtYrplZr5pZsJtYrppZr5pZsJlarppZr5tZr5parppZr5lZr5pasJtZr5pYr5pasJlZr5pYr5tasJpZrppYr5lZrptZr5pYr5pZr5tZsJpar5tZr5par5lZr5pZsJtZr5pYr5lZrppZr5pYr5pZrppYr5pZsJpZr5pZsJtZr5pZr5pZr5par5tZr5pZr5pZr5pZr5pYsJpZr5pZr5tYsJpYr5pZr5par5pZr5pZr5pZrppZr5pZr5lZsJpZr5lZr5pZr5pYr5pZr5pZr5lZr5pZr5par5pZr5pZr5pZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5par5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r////kUHlRAAAAv3RSTlMAAQIDBAUHCAkKCwwODxAREhMUFhcYGRwdHh8gISImKCkrLC0uMDIzNDU2Nzk7PD0+QUJDREVGSEpLTE1OT1JWV1hZW1xeX2BhYmRlaGprbG1ub3JzdHV4eXt8fn+AgoOEhYaHiImKi4yNjo+QkZKTlJWWl5iZm52foKSlpqipqqytrq+wsbO0tbe4uru8vcDCw8TFxsfKzM3P0NHS09fY2drb3N3f4OHl5ujq6+zt7u/w8fLz9PX3+Pn6+/z9/jP3eMQAAAABYktHRMATYd/4AAADFElEQVQYGZ3BCV9UVQDG4T8DmCRWgKgVlpZlq9FCGGipWURlRYsSaEpSikoamhi2UYGCtltSShalZkRRuASMvF+tmXPOwJ1h7uXXeR5mdu+Og93Hfx5ei58GGbX4aZTxPH42ydiAn80yNuHnNRnN+NkioxU/W2UcwM/rMjrws01GJ36aZfTg500ZX+Bnu4zv8LNDRj9+dsr4FT+7ZJzHz24Zf+PnLRmj+NkjKw8ve2XNwUubrBK87JN1A17ekbWY/6G4asPejsPHujr2/SBrGVbpKz39p7rrigmT/0hr/4QyLScp1nBJxj8v5ZDN9TsHlU0FCbkfadK7OUxTtH9U2a0iYbsCXiXTY4MKswZYHFfA5YWkiW1TuFpgj9I0EZTXqQjPAQNKc4KgdkWpg9iY0vxFQI0irYe8K0pzkYBeRXoGOK80p5lSGlekGqBDadqYUqdojwMPKmjiLqZ8q2irSehWwHtMKZtQtEdJuPZHTfp+DlPqlaGvqaaivPLJN4ZlVZN0Xa+czkICTirdZzgnZFVirfh4RBr+4H6ClijDxdlYX8l6iElzC8nQpEwrsY7KKifCgDK1Y3XJuodwd2uaoTyM92XdSbjdmq4c46Cs2wgV+03OhV6ltGC0ybqFUA8oZX+FUs5gtMi6iVAHlHJ7zoBSlpK0VdYiwuQPyfkG6pXSSFKDrIWEqVbKOpg3JqePpJdllRLmkJw/rgI6lVJGwnpZRYQouCCnmYRKpdSR8JSsuYRYLedKGQk5v8j5nIQ1sgoI8amcIxgNcuLFwCpZ+WR3zb9yqjBKx+Q8ATwsK4fsauWcy8X6RE4ncJ+McUIck7MRZ4WcSwVwh4zLZFcyLmt0Hk7srJyVsETGCNm9IOcQkxrltMMiGX+S3XE5y5k0f1zWUB7zZfxOVjdOyDpFQJeccq7+0NhFVhvlPEtAlZwWop2UNVJIQOysrDNEulnO26TZLGcpUbbIuZU0C+KyGolyWtaXZOiW1UeUBfU/KWktGapljPXMItqy1iENziJD7jkp/vWLJcxs9roapmk8+nQRM/gP/N1HWzZygPsAAAAASUVORK5CYII=',
    link: 'PhysicalScreen',
  }, {
    id: '2',
    title: 'Pain / Injury',
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACoAAAA9CAMAAADszgtKAAAB4FBMVEUAAAAA//+AgIBVqqpAv4BmmZlVqqpJtpJgn59Vqo5duaJVqpVisZ1VqplatJZVqpxZs5lVqp5drpdZsZtVqpVcrZlYsZ1YsJ5Vs5larZxYr5ddsptarZ5Xr5lcsZxarJhXrppcsZ1ZrJlZsppXrpxar5pYsZxar5tXrZpar5xYsJlbrZtYsJpbrZxZrplasZhYr5tZrptasJpXr5lasJpasJtbrppZr5pZr5pYsJtZr5tZr5tYr5pasJtar5tZsJlYrppar5tar5lZsJpar5lZr5pYrplar5pZr5tYsJlZr5lYsJpZrptZr5lasJpZrplasJtZrplar5lZsJpYr5tar5pYrppZr5pZsJlZr5tZr5lasJtasJlZr5pYr5lZr5pZrplZrppZr5lZsJpar5tZr5pZsJpZr5pZsJtYr5pZr5lZsJpZr5par5pZr5pZr5par5tZr5pZrppZr5pZr5pZrptZr5pYsJpZr5pZr5tZr5pZr5pZr5par5pZr5pZr5pZr5lZrppZr5pZr5pYr5par5pZr5lZr5pZr5pZsJtZr5pZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r////bITSmAAAAnnRSTlMAAQIDBAUGBwgJCwwNDxESFBUWFxgZGh0eHyAhIiMkJSYnKCssMDEzNTY3ODo7PD5AQkRGR0pMU1ZXWVxdXmNkZWZpamxtbm9wcXN0dXZ3eHp7gIGCg4iJioyPkZSVmZ2eoaOkpaanqaqwsrS1tri7vL2+v8DBw8TFxsjJzM3Q0tPU2drb4eLj5efo6err7O3u8PHy8/T3+Pn6+/z9/gkOedQAAAABYktHRJ/otZOdAAACIklEQVRIx6XUZ1sTQRQF4BNAsQuIUcDeiRrFhthjb4AV+2o0iKigEQRRwUJAsYHBaFiy57f6IdnsJrkzWR/P53fnmdl7ZoD81JzrfBPrb98/D0WystNiOomL5Vp5LEkn75do5DXmZKROKU8yL29Ve9ho5lNeVdDnBZKJSlFuoZDzIr0p0V6RDkk0WSLRCYmySqJJkYq/dkSkcyTaK8kf4rGaJfpIpMunBXpQntbdQjk6U6b+bwX0gKpZW//kyYi6rzvGc2THLE23l3U58FerT8WWHikBsC38nST5sW0xgIa+llX5rnRvv5VpXOmK4J7NfgBAMEEyujtn9cYYSaYO537fOJneyktn5ersDiM1DqyLZLf9u8m+U5+cs5hPTwRXL5xdVX/0iXt4qUMAgPVxkomHJKfkHpJkchOAyjEyfmZ+BUmrS0k5CCBMxtcBFSR5Wk25E7UWeRkZGphU09toJVlv04awmg7hAUm/TZt2qelXREnOtenxsi/qf4AeMuWz6SXcUlITPaQJm95BwCvtAIY90hfAFY/0HbDGIx0DMOiNmj7grDfKBYB/2hu9EAqFRr1RTf6D9hmGYRjG/Wc/i9Jm+w6W7ftchF53LuyimJ7ec70CAUtLH7tfjNdaOuCmN7R02E1btHTCTdu01Jrhot36aVU7sjapp2uzsjxapAPbbblhQJpWyjAMI/OudKdL0P5B7oC3/BONeqVTODXukb76C8YK3EMAbkxiAAAAAElFTkSuQmCC',
    link: 'PainScreen',
  }
  ]


const Activity = () => {

  const [isVisible, setIsVisible] = useState(false);
  const [selectedData, setSelectedData] = React.useState([]);
  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');

  const [questionsData,setQuestions]=React.useState([]);
  const [optionsData,setoptionsData]=React.useState([]);
  const [PhysicalTabTitle, setPhysicalTabTitle] = useState('');

  const [PhysicalTabTitle2, setPhysicalTabTitle2] = useState('');
  const [questionsData1,setQuestions1]=React.useState([]);
  const [optionsData1,setoptionsData1]=React.useState([]);

  const [testResponse, setTestResponse]=React.useState({});

  const [demographyFlag, setDemographyFlag] = useState(false);
  const [healthhistoryFlag, setHealthhistoryFlag] = useState(false);
  const [foodMoodFlag, setfoodMoodFlag] = useState(false);
  const [activityFlag, setActivityFlag] = useState(false);


  useEffect(() => {
    // console.log(userId)
    AsyncStorage.getItem('authToken').then(
      (value) =>{
      setAuthToken(value);

      AsyncStorage.getItem('UserId').then(
        (value1) =>{
          setUserID(value1) 
          axios(`http://18.139.75.180:3000/sunev/api/v1/user/user_questionnaire/${value1}`,{
            "headers": {
            'Authorization':value
           }})
            .then((response) => {

              setDemographyFlag(response.data.data.is_demography_complete)
              setHealthhistoryFlag(response.data.data.is_healthhistory_complete)
              setfoodMoodFlag(response.data.data.is_foodmood_complete)
              setActivityFlag(response.data.data.is_activity_complete)

              // setTestResponse(response.data.data);
              // Object.keys(response.data.data).forEach((key)=>{
    
              //   // console.log("key3: ",key);
              //   if(key === 'activity'){
              //     // console.log("key: ",response.data.data[key]);
              //     Object.keys(response.data.data[key]).forEach((activity)=>{
              //       // console.log("key2: ",response.data.data[key][activity]);
              //     });
              //   }
              // });
    
                let physicalActivities = response.data.data.activity.Physical_Activities
                setPhysicalTabTitle(physicalActivities[0].section_title)
                setQuestions(physicalActivities[0].questions)
    
               activityAnswer=response.data.data.activity
    
    
                let PainInjuryList = response.data.data.activity.Pain_Injury
                setPhysicalTabTitle2(PainInjuryList[0].section_title)
                setQuestions1(PainInjuryList[0].questions)
    
              
    
                // response.data.data['activity']['Pain_Injury'][0] = questionObject1;
              
            })
            .catch((error) => console.error(error))
           
          }
        );
        }
      );

     
    LogBox.ignoreLogs(["VirtualizedLists should never be nested"])
  }, [])


  const renderItem1 = ({ item }) => (
    <TouchableOpacity style={{margin: 20, flexDirection: 'row',justifyContent:'space-between',paddingEnd:10, borderColor: '#59af9a', borderWidth: 2, borderRadius: 10, alignContent: 'space-around', marginBottom: 10, marginTop: 0
      }} onPress={() => setIsVisible(!isVisible)}>
      <Text style={{fontSize: 20, color: '#59af9a', alignSelf: 'flex-start', margin: 20,fontWeight:'bold'}}>{item.title}</Text>
      <Image source={{uri:item.photo}}
        style={{width: 25, height: 25,marginEnd:10, alignSelf:'center',justifyContent:'flex-end'}} />    
    </TouchableOpacity>
  );

 

  const renderItem2 = ({ item, index }) => {
    return (
      <View   style={{width: '90%'}}    >
               <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5}}>{item.title}</Text>
                  <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
                    <TextInput  style={{flex:1, color: 'black'}} />
                    <Image source={require('../../Assets/icons/expand-plus-icon-green.png')} 
                    style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
                  </View>  

                <FlatList 
                    data={item.options}
                    scrollEnabled={true}
                    numColumns={7}
                    listKey={(item, index) => 'D' + index.toString()}
                    columnWrapperStyle={styles.wrapView}       
                    style={{width: '95%', margin: 5, marginTop: 5}}
                    renderItem={renderItemInsideOptionsRadio} />
      
      </View>    
    );
  };


  const renderItemInsideOptionsRadio = ({ item }) => {
    const { title, slug } = item;
    const isSelected = selectedData.filter((i) => i === title).length > 0;
    return (
      <TouchableOpacity style={{borderColor: '#59af9a',backgroundColor:'white', height: 35, borderWidth: 1.5, color: 'black', alignSelf: 'flex-start', borderRadius: 5, marginLeft: 5, marginRight: 0, marginBottom: 0, marginTop: 5, justifyContent: 'space-evenly'}}>      
      <Text style={{fontSize: 15, color: 'black', textAlign: 'center', marginLeft: 15, marginRight: 15}}>{item.title}</Text>
    </TouchableOpacity>   
    );
  };

  const handlleChange = (index,questionIndex) => {
    activityAnswer.Physical_Activities[0].questions[questionIndex].options[index].isSelected=true   
  };

  const handlleChangeUncheck = (index,questionIndex) => {
    activityAnswer.Physical_Activities[0].questions[questionIndex].options[index].isSelected=false 
  };

 

  const renderItemInsideOptions = ({ item ,index},questionIndex) => {
    
    const { title, isSelected } = item;
    // const isSelected = selectedData.filter((i) => i === title).length > 0;
    return (
      <TouchableOpacity
      onPress={() => {
        if (isSelected) {
          setSelectedData((prev) => prev.filter((i) => i !== title));
          handlleChangeUncheck(index,questionIndex) 
        } else {
          setSelectedData(prev => [...prev, title])
          handlleChange(index,questionIndex)
        }
      } }
      style={[{borderColor: '#59af9a',backgroundColor:'white', height: 35, borderWidth: 1.5, color: 'black', alignSelf: 'flex-start', borderRadius: 5, marginLeft: 5, marginRight: 0, marginBottom: 0, marginTop: 5, justifyContent: 'space-evenly'}, 
      isSelected && { backgroundColor: '#59af9a'}]}>
      <Text style={{fontSize: 15, textAlign: 'center', marginLeft: 15, marginRight: 15, color: isSelected ? "white" : "black"}}>{item.title}</Text>
    </TouchableOpacity>  
    );
  };


  
  const renderItem3 = ({ item, index }) => {
    return (
      <View style={{width:'100%'}}>

              <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginTop: 0, marginBottom: 0}}>{item.title}</Text>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
                  <TextInput  style={{flex:1, color: 'black'}} />
                  <Image source={require('../../Assets/icons/expand-plus-icon-green.png')}  
                    style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
                </View>  

                <FlatList 
                    data={item.options}
                    scrollEnabled={true}
                    numColumns={7}
                    listKey={(item, index) => 'D' + index.toString()}
                    columnWrapperStyle={styles.wrapView}       
                    style={{width: '95%', margin: 5, marginTop: 5}}
                    renderItem={(childData) => renderItemInsideOptions(childData, index)}
                    extraData={item.title} />
      
      </View>    
    );
  };

  return (
    <View style={{flex: 1}}>
      {!isVisible ?
        (
          <View style={{flex: 1, marginBottom: 20}}>   
            <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: demographyFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: healthhistoryFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: foodMoodFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: activityFlag ? '#59af9a' : 'white',marginStart:5}} />
            </View>   
            <View style={{backgroundColor: '#D3D3D3', width: '90%', color: 'black', alignSelf: 'flex-start', padding: 10, borderRadius: 10, borderTopLeftRadius: 0, margin: 20, marginTop: 10, justifyContent: 'space-evenly'}}>
                <Text style={{fontSize: 18, color: 'black', textAlign: 'auto', marginLeft: 5,padding:5}}>{StringsOfLanguages.Thirtyone}</Text>
            </View>
            <FlatList 
              data={DATA1}
              scrollEnabled={true}
              renderItem={renderItem1} />
            <View style={{alignSelf: 'center', margin: 20, marginBottom: 5, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 15, color: '#7e7e7e', textAlign: 'center'}}>{StringsOfLanguages.TwentySeven}</Text>
            </View>
            <TouchableOpacity style={{borderColor: '#e7973d', width: '90%', height: 50, alignSelf: 'center', borderWidth: 2,
              marginBottom: 0, margin: 15, marginTop: 0, borderRadius: 10, justifyContent: 'center'}} onPress={() => navigation.navigate('Dashboardpage')} >
              <Text style={{textAlign: 'center', fontSize: 20, color: '#e7973d',fontWeight:'bold'}}>{StringsOfLanguages.Nineteen}</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      {isVisible ?
        (
          <ScrollView>
            <View style={{flex:1,marginTop:10,marginStart:10,marginEnd:10,marginBottom:8}}>
            <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            </View>
              <Text style={styles.text1}>{PhysicalTabTitle}</Text>
              <View>
                <View style={{marginBottom: 0, justifyContent: 'space-between'}}>
                  <FlatList 
                    data={questionsData}
                    scrollEnabled={true}
                    numColumns={7}
                    columnWrapperStyle={styles.wrapView}       
                    style={{width: '95%', margin: 5, marginTop: 5}}

                    renderItem={renderItem3} />
                </View>
                           

                  
                <Text style={{fontSize: 20, color: 'black', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5}}>{PhysicalTabTitle2}</Text>     
               
               
                <View style={{marginBottom: 0}}>
                  <FlatList 
                    data={questionsData1}
                    scrollEnabled={true}
                    horizontal        
                    renderItem={renderItem2} />
                </View> 


                <TouchableOpacity onPress={()=> {
                   axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_activity',{
                    "user_id" : userId,
                    "activity" :activityAnswer
                  },
                  {
                   "headers": {
                   'Authorization':authToken
                  ,'Content-Type': 'application/json',
                  }
                  })
                  .then((response) => {
                    ToastAndroid.show(response.data.msg,ToastAndroid.SHORT);
                  })
                  .catch((error) => {
                   console.log("ERROR:::  " + error.message);
                }); 
              
                }} style={{backgroundColor:'#59af9a',width:'95%',height:50,alignSelf: 'center',
                  marginBottom:15, margin:15, borderRadius: 10, justifyContent:'center'}} >
                  <Text style={{textAlign:'center',fontSize:22,color:'white'}}>{StringsOfLanguages.FourttyThree}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        ) : null}
    </View>
  )
}
export default Activity;

const styles = StyleSheet.create({
  text1:{
    alignSelf:'flex-start',
    fontSize:20,
    paddingLeft: 15, paddingRight: 15,
    color:'#000000',marginBottom:15
  }, 
  text2:{
    fontSize:20,
    color:'#000000',marginTop:10,marginStart:13
  },
  input: {
    height: 60,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:3,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  itenText:{
    fontSize:18,color:'#59af9a',
    fontWeight:'bold',paddingStart:15
  },
  inputEthnivity: {
    height: 120,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:5,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  wrapView: {
    flexWrap: 'wrap',
  },
});