import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, FlatList, TextInput, Image, TouchableOpacity, LogBox, ScrollView,ToastAndroid } from 'react-native';
import StringsOfLanguages from '../../Constants/StringsOfLanguages';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';


let activityAnswer={};

const DATA1 = [
  {
    id: '1',
    title: [StringsOfLanguages.ThirtyEight],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADcAAABICAMAAABlcZ1gAAACN1BMVEUAAAAA//+AgIBVqqpAv4BmmZlVqqpJtpJgn59Vqo5Ns5lduaJVqpVisZ1btpJgr59atJZVqpxdrpdZsZtcrZlYsZ1Vs5dbrZtYsJ5Vs5lYr5darZ5Xr5lcsZxarJhcsZ1ZrJlbsJhZsppbsJlZsZtXrphar5pYsZxXrZlYsZhar5xYsJlYsJpbrZxZrplYsJtasZhZrppYr5tYr5hasJpZrptasJpXr5pasJtYrZlbrppYsZparptarptYsJtarplZr5pYsJlarppZr5tYr5pasJtZrplar5pYrppar5tZsJlYrppar5tar5lYrptZr5pYrplar5pZr5tYsJlZrppZr5lZr5lZrplZr5pasJtZrplar5lYr5tar5pZsJtZr5pZsJtYrppZr5pZsJlarppZr5tarppZr5pZr5pYr5pZr5pYr5tasJpZrppZsJpZr5pYr5pZr5tZrppar5pZr5lZsJpar5tZr5pZr5pZrppYr5lZrppYr5pZrppZsJpar5pZsJtZr5pZr5pZr5pZrppZr5pZr5pZr5pYsJpZr5pZr5tYsJpZr5pYr5pZr5tZr5pZr5lZr5pZr5pZrppZr5pZrppZr5pZr5pZsJpZr5lYr5pZr5pZr5lYr5pZr5pZr5par5pZr5pZrppZr5pZr5pZsJtZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5par5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r///8EEQ+rAAAAu3RSTlMAAQIDBAUGBwgJCgsMDQ4QERIWFxkaGxwdHiAiIyQlJygqKy0uLzAxMjQ2Nzo7PD0+P0BDREVHSUpLTE5PUlRVVlpbXF1eX2BiY2RlZmlrbW5vcHFyc3Z4eXp7gIKDhIaHiImKi4yOkJKTlZaXmJqdn6ChoqOkpaapq62usLG0tre6u72+v8DDxMXGx8nKy8zOz9DR0tTV1tfY29zd3t/g4ePk5ebn6Onr7O3v8PHy8/T19vf4+fr7/P3+6TSK8wAAAAFiS0dEvErS4u8AAAMASURBVEjH7dXXQ9NQGAXw0xYtVlRELSiKe4MgYt2KqLiKEzduUXFvtKJiXaAiKsVZlLpB0boq9PxzPtAkN2nTptE3PW/5vvtrbnJvboFIbAev5wBAyQ0XABTXL4GhzCUrAWSH2AAg7QMDxtxzcjGAGnIvgB3kZUNsENnRC7C0kcMBeElj88wlfQAc5C8bAB/pNuTGkZ2TAUuQLAVwnTysHdKr+Exd07XDs3uIRWsL+dACVJJt/YDVZFeems1rZXeeThfL47vIsYDVQ5YAKQ9Ij4ptCVNK51KxcZucD6CC3AZgLdkqdksp5NckpWFpIV0ALpEbAOwiHwlsYFB0vK908skfacCAEDkOsAbI1YKrpDoFcmc9WQ1gOtkUWc40YTLvNG6/MpObz7MB2D2P8wDgQEuJcLuxGsY7xjbhAq17Y8yVad2PqCFzKoqi3VKt+yR289cV2k6QPHmx+fxIlZuidT5hp50mwz6p8WWE6GzvNe6A0lum7pxS3XCzuhkeI6/Qqlfq1uc96YJL9en86H5G5bFDgINbxW0mb4mMzmjHteJMM7xy/UIfudqvKEbU7xQza76RbK8uQLJJcY7KEK/TdeOI9zMe6ibkEsblbD53vHyY6kvXzSLlxVR3nxRXJ0iV/g26bIvMsv1S7WeZVOt7K7YKl8vM0SyU5XOptzcW6ypTnmW72Ag6pXLPc9Hs6zzhzO3QOSYsO7Xs9UThVc5Q914ILfdPVas+U1yqjZoHTxV6o4/c9Ufy5MpCi2qJ92kmk2lwh2k/l6w/cFPdcVLq1HPbGDftw2K71O/xHQ/FdkMTMHpjO1t9fBZervN89hW7lRwlyZdCoSLX0DoMJclrya/f33ZVSTp70Uq32+0u/5CccwV03nJ8lx+iKfeQplwOzblpJp3r7zunOReym3NemHLhQnNuKwy7gHS0+p/VzoJhF7LGPyf1XBD/3T/jchX3NoHLIsnayEWfr7KrSeAsfpKbpKs1EusYkeiPfPy9j8eUj3nm2YbGxsa6qiFR434DapLiT5gleOMAAAAASUVORK5CYII=',
    link: 'EatingScreen',
  }, {
    id: '2',
    title: [StringsOfLanguages.ThirtyNine],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADMAAABACAMAAACAyb93AAACBFBMVEUAAAAA//+AgIBVqqpmmZlVqqpJtpJgn59Vqo5duaJVqpVisZ1btpJatJZVqpxerpRZs5lVqp5drpdZsZtVqpVcrZlVs5dVs5lYr5ddsptarZ5csZxarJhXrppcsZ1XrptZsppbsJlXrphYsZxXrZlbrZtZrphYsJpbrZxZrppYr5tasZlZrptZrptXr5lasJpZrphXr5pasJtYrZlbrppYsZparptYsJparptZr5pYsJtarplYsJtZr5tYsJlarppZr5tasJtZrplar5par5tZsJlZsJlYrppYrptar5lZr5tZrppZr5lYsJpZrptasJpZr5pasJtYr5par5lYr5tar5lYr5tZsJtZr5pYrppZr5pZsJlarppZr5tZr5parppZr5lasJtYr5pasJlZr5pasJpZrppYr5lZsJpYr5pZr5pZrplYr5par5pZsJpar5tZr5pZsJpar5lZr5pZsJtZrppZr5pZr5lZsJpZr5par5pZsJtZr5par5tZrppZr5pZr5pYsJpZr5pYr5pZr5tZr5par5pZr5lZr5pZr5lZrppZsJpZr5pYr5pZr5pZr5lZr5par5pZr5lZrppZr5pZsJtZr5pZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5r///+VteHbAAAAqnRSTlMAAQIDBQYHCAkLDA0OERITFBUWFxgZGx4gISIkJSYnKSstLzEyODk6Oz9AQUJFRkdISUpLTE5PUVJTVFVXWVpbXF5fYGNkZ2hrbHByc3R1d3l6fH1/gIKEhoiJiouMjY6PkZOUlZeYmZqcnZ6foqSlpqeoqaqur7K0tba3u7y+wMPHyMrLzM3O0NPU19nb3N3g4eLk5ufo6err7O3u8PHy8/T29/j5+/z9/hyasBgAAAABYktHRKvJAWcoAAACYUlEQVQYGc3B+SMUYQAG4HdXRUiXTUW0rkqlC4kSiUhKlyvUKo1Oikp3iXRRCtvlSqx9/8pm7/lmPzvzY8+DEHvT0Pf5r48r42DW6ruL9HEehzlbRhiiWGFCwjC1mmDCPYpyYCjDTVE/DLVRLxVG3lOvGkZmqHcJBiwu6l2HkXHqXYCRZ9Q7BCNV1JmLhZHYCYquwFgFBeNrYEIHNeayYYbljJsBY1kwKfslvWZbVsG8pIp6R11eDP43ltQdhcURHIiDztbOHzTwHIKohgUaaoeWtYsmZEGrhiaMWKARP0MT6qFVSbk/vzXepkDrDmWGc6KwtEFKLKQikjFKDCGSKBclniASG2V6obFxV9GxI/uSEZRBmVvwiyvvm6LPz5tZ8NlPGQe8rFVOavXa4FFBmQZ4RN+nzmgKVOcoUwePBwzzeSWAa5SphqqEEm0AHlKmHKo3lJiKAV5TpgjAJkrlAR8oswfAbkqdBG5QZjuAMko1A/GOwWmGSQewl1Kn4GXLmaBoM4DlnygxaYPfO4rWQZXYoIRpTUPAK4piYKyPAhdUmS2KhCMXfl0UTAKwz1OuED4dFHwDcJ5L6IFPEwUfARzmEurhc5aCAQCW1l+UmO2OhU81BS9gQhkFj2BCAQXd8EhXesPczkXATgoUqDZMU2JxG/wyKWiD6iilGuGXTMFFqPIpVQe/tRTUQpVBqVL4raDgBFTWL5RYWI+Av9QqhUctJboQ5KRWATysTxlmNAFB/dSywyu63U3RQBJC8l0M6UGAvdPJIPdg2TJopTcqfldLrNBIK65pVhTl8umDifD5B+lkXOMsF2W6AAAAAElFTkSuQmCC',
    link: 'PreferenceScreen',
  }, {
    id: '3',
    title: [StringsOfLanguages.Fourtty],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADwAAAA8CAMAAAANIilAAAACNFBMVEUAAAAA//+AgIBVqqpAv4BmmZlVqqpJtpJgn59Vqo5Ns5lduaJVqpVisZ1gr59atJZVqpxerpRdrpdZsZtcrZlVs5dYsJ5Vs5larZxYr5ddsptarZ5Xr5lXrppcsZ1ZrJlXrptZsppXrpxZsZtXrphXrZlar5tXrZpar5xYsJlbrZxasZhZrppYr5tasZlZrptYr5hZrptXr5lasJpasJtYrZlbrppZsJhYsZparptYsJparptZr5pYsJtarplZr5pZr5tYsJlarppZr5tYr5pasJtZrplar5pZsJtYrppar5tYrppar5tZsJlYrppar5lYrptZr5pYrplZr5tYsJlZrppZr5lZrptZr5lZrplZr5pZrplYr5pZrppar5lZsJpYr5tar5pZr5pZsJtYrppZr5parppZr5tZr5parppasJtYr5pasJlZr5pYr5tZrppYr5lZrptYr5pZr5pZrplYr5pZr5tZrppZsJpZr5pZrppZr5pYr5lZr5pYr5pZrppZr5lYr5pZsJpZr5pZsJtZr5par5pZr5pZrppZr5pZr5pZr5pZr5pZr5tYsJpZr5pZr5pZr5tZr5par5pZr5lZr5pZrppZr5pZrppZr5pZsJpZr5lZr5pYr5pZr5lYr5pZr5pZr5par5pZr5pZr5pZr5pZsJtZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r///8kmImGAAAAunRSTlMAAQIDBAUGBwgJCgsMDRAREhMWFxkbHR4fICEiIyYnKCkrLC4vMjM1Njc7Pj9AQUJDRUZHSktMTU5PUVJTVFVWWVpbXF1eX2BhYmNlZmdoaWttbnBxcnN1dnh5e3x+gIGCg4aHiImLjI2OkZOUlZaYmZucnZ6foKGkqausra+wsbKztLW3uLm6vr/AwsPGx8jJy8zNzs/R0tTV19jZ293e3+Dh4+Xm5+jp7O3u7/Dx8vP29/j5+vv8/f6m56ekAAAAAWJLR0S71LZ3TAAAAvpJREFUSMed1/1fDEEYAPBn76o7TnEuQnEUl5e8RV5S7i4v5SXlJUkpikInLyGVKKQrlcp7okTRIYXo5q+zt7ezM7t7fczc89M+zz7f27vZmdk9gP9HpDU+LgLCi9xRhNBU+4FIfircQnK8WMKNi5ES7+Zx2thJgtFFTlxAWTRh5sONNEbr+XCnCrv5cIsKb+fDJbT1L+TDceMUbue9zy6/Yv8kc8+SAqynD4Uxt3eNSHY4LayVMSe7tumq0wSwaOdGc3iLKyq9eRoh394w6BqPT/7pObx0dz+5Xz8W8Nk81TSr5bJbp1X4e5SuY7kdTroha5XeJnxF6tis7bD0JJp+u4XRffpR7tVY5NG2lH4xpKKkbcimw1Vaiz4KmpYUtEe49ME2cUprt/h1GDm0TTUXoPYuNLUpu+Zq92Excj/rLTqnsdFvXdBSBAN4jzM8VPfX0ckr7ZXTLZBkhXWxgeO0ijuaUbppGKTTeOKsDlU4n+q+57NZcIbOTxB8Rdt77+CGLDr/JM7IOHqmdBDs1FifRfzND0g+Lo1uK9Xx16pgw0s1LgsUTYXePil66+z6S+ynnik1fSS6SkM/Tk1jFG7kXZeXKTxIf2q8EjNvzSsp3E/Ve6h63oxbmY80VZGymR6L+hns7A7qwjHBWnOIuYuminTW3EZNz1i5iEJGnW5VU3O9ey5wYWM9OeeNBi48/xE51WACLix0kzM3jAC2ZRyYusOV4ha0YqiEA2cq9fNitukbesyB8/GbwXExyZkSX4si2HEZqQrl0lEyO75GNr7q4FE5O74vV/MBXgePfi1lxnjdZADgR08rMx6Sq2vBqLwc7WDFP+VqAsBp3DFsYcMxuCr2W0bpnY4B2/EuGkiO4pbAmDHgFLk4IK3N94iMGQPGm26nlGUjMmYM+JhqgzK+wU3XWfBZuVgdTDNwUxMLLiYTTFrdfXJewYId8kvvYjlPlR9YieAPiW+rJolHqhUqeSVeHA2h9GSmChuOPB/rov8ipHtHnrgA/gECd1bB9BsjwgAAAABJRU5ErkJggg==',
    link: 'CookingScreen',
  }, {
    id: '4',
    title: [StringsOfLanguages.FourttyOne],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAD0AAAA2CAMAAABDe2EYAAACRlBMVEUAAAAA//+AgIBVqqpAv4BmmZlVqqpgn59Vqo5duaJVqpVisZ1Vqplgr59atJZVqpxerpRZs5ldrpdZsZtVqpVYsZ1Vs5dVs5larZxYr5ddsptarZ5Xr5larJhXrppcsZ1XrptbsJhZsppZsZtYsZxXrZlYsZhXrZpYsJlbrZtZrphYsJpbrZxZrppYr5tZrptYr5hasJpZrptXr5lasJpZrphXr5pasJtYrZlbrppZsJhYsZparptZr5larptZr5pYsJtarplZr5pYsJtarppYsJlZrplZsJtYrppar5tZsJlYrppar5tZsJlar5lZsJpYrptZr5pYrplar5pYsJlZrppZr5lYsJpZr5lasJpasJtZrplYr5par5lZrppYr5tar5lar5pZsJtYrplZr5pYrppZsJlarppZr5tarppZr5pasJtZr5pYr5pZr5pYr5tZrppYr5lZsJpZrptYr5pZr5pZrplYr5pZr5tZrppar5pZr5lZsJpar5tZr5pZsJpar5lZr5pZsJtZrppYr5lZrppZrppYr5pZr5par5pZsJtZr5par5tZrppZr5pZr5pYsJpZr5pZr5tYsJpZr5pZr5pYr5pZr5tZr5pZr5pZr5lZrppZsJpZr5lZr5pYr5pZr5pZr5lYr5pZr5par5pZr5lZr5pZrppZr5pZsJtZr5pZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5par5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r///9hMfyrAAAAwHRSTlMAAQIDBAUGCAkLDA0PEBESExQWFxgaGx4fICEiIyUmJykqKy4xMjQ1Nzg5Ojs/QEJDREVGR0hJSktMTU5PUFJTVFVWV1haX2FiY2RlZmdpamttbm9xcnN0dnd6e3x9fn+Ag4SFhoiKi4yOkJGSk5WWmJmam5ydnp+goaKjpKWmp6ipqqutrrGztba3uLy+v8DExcbHyMnKy9DS09TX2Nrb3N3e4OHi4+Tm5+jp6uvs7e7w8fLz9PX29/j5+vv8/f5x6C3vAAAAAWJLR0TBZGbvbgAAA09JREFUSMe1ludXE0EUxS9BggWQKGIXKYoFMZGiWBIFwYqoKCiKooAFG/beRVFUbBRFRVA09tWgEBLh/ml+2E02yW42G8/xfnpv3vvN2Zl5+2YAVRlSbeX1m40Ahq09Wm5LNUC3DAsuCSTJGwCukSSFi2Z9E0Tkv6ZHxSj22i+tOuCUNsr6vfS3j/d4aih4jW86Oejn/czXZCNPUFs1Wks+z1DaF5zew9AqCQbnDkkZfTfKcnILa5rdJO21tqTpszc1DEixgTR1OOajGP+8JU4aMa2uKzBKdkJVrxh+PkyVPkiSdB+ICfJp4y6K+Ea1YEI/STrytI5zgCQ/RauEaknSnix5UdaD9ZVmAJhTtdMSIY3O7yPJIpWj/kjSOVPykjtIsg3APpJs81RZAUk2K2kLSe6XnERxAzsB3CFJfkiUIrdJDo5T0BtJUsgSHWl/ZJrXxUDeL5LMVNATXCTpPmwCMKpfQbtNAMafGSJJYZTy00+KaT+SgBlU0LQAWdKkFSp7Hv9JjM0BzCp0AbBKtF4Y1U5znkuLXumh7RPVi8Hm1kE70oLVUubnkHTPzOClOLk9BN0Ur9VcokodGvS30lCNNTFBprsB3JPpqTG6mrKH/jMNcV9lWqc8NIWrbxk2naHSz6y66SQVepZuerhTAQ+N1X8PNivoVv0wVivosjBoY3cA/H54GDRmu/xXvQxhyea3cbsQpua+8rLuUoSt6NIuEb6fjn+RIaPmyvEtKfiPEq+Z2GVFC4166jAjb/4ImTvUHgsY97pJ2i9Ulqw0Ba+C9A2n290kXUfigJEt5wC8YWda5F35dKYA6R0PLtftLl+fBSDdYrGuKNq6NxVIlZOejym2sy8asUNk/y15/HsEUCjZzwA8lewdQGSvnOYkSbPcCSQ1AahQoRsAPAzIrUZZwMgxAKdU6B4A9QG5Ld5Mjx5FYbHnhdMGoNUTsAE3A3IFZJ/cvqTBd+hLp9fsTcBY71oHW+76viPrlu88lQsAKAn2OHt3pidYaJ58ff5kuOowaPWSEHLO9Xt3OMOCv2X7V+HEmicOnWj/o22jVf6WSUsqrrV+0QIdrcfXpUVp/UQjUhZtqKy70Pj0dZddEP7QJQjdXe2NZ6s350wIzP0L7lYPp0ofYo8AAAAASUVORK5CYII=',
    link: 'CravingScreen',
  }
]



const FoodMoodpage = () => {
  const [isVisible, setIsVisible] = useState(false);
  const [selectedData, setSelectedData] = React.useState([]);
  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');
  const [isLoading, setLoading] = useState(true);

  const [foodMoodTitle, setfoodMoodTitle] = useState('');

  const [questionsData1,setQuestions1]=React.useState([]);
  const [questionsData2,setQuestions2]=React.useState([]);
  const [questionsData3,setQuestions3]=React.useState([]);
  const [questionsData4,setQuestions4]=React.useState([]);

  const [demographyFlag, setDemographyFlag] = useState(false);
  const [healthhistoryFlag, setHealthhistoryFlag] = useState(false);
  const [foodMoodFlag, setfoodMoodFlag] = useState(false);
  const [activityFlag, setActivityFlag] = useState(false);

  useEffect(() => {
    console.log(userId)
    AsyncStorage.getItem('authToken').then(
      (value) =>{
      setAuthToken(value);
      AsyncStorage.getItem('UserId').then(
        (value1) =>{
          setUserID(value1) 
          axios(`http://18.139.75.180:3000/sunev/api/v1/user/user_questionnaire/${value1}`,{
            "headers": {
            'Authorization':value
           }})
            .then((response) => { 
              
              activityAnswer=response.data.data.foodmood

              setDemographyFlag(response.data.data.is_demography_complete)
              setHealthhistoryFlag(response.data.data.is_healthhistory_complete)
              setfoodMoodFlag(response.data.data.is_foodmood_complete)
              setActivityFlag(response.data.data.is_activity_complete)
    
              let foodMoodObject = response.data.data.foodmood.Eating_Habits
              setfoodMoodTitle(foodMoodObject[0].section_title)
              setQuestions1(foodMoodObject[0].questions)
    
              let foodMoodObject1 = response.data.data.foodmood.Preference
              setQuestions2(foodMoodObject1[0].questions)
    
              let foodMoodObject2 = response.data.data.foodmood.Cooking_Eating_Out
              setQuestions3(foodMoodObject2[0].questions)
    
              let foodMoodObject3 = response.data.data.foodmood.Cravings_and_stress
              setQuestions4(foodMoodObject3[0].questions)
    
             
    
            //  console.log(response.data.data.foodmood.Eating_Habits[0].questions[0].options)
    
            })
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
          }
        );
        }
      );

    
    LogBox.ignoreLogs(["VirtualizedLists should never be nested"])
  }, [])

  const renderItem1 = ({ item }) => (
    <TouchableOpacity style={{margin: 20, flexDirection: 'row',justifyContent:'space-between',paddingEnd:10, borderColor: '#59af9a', borderWidth: 2, borderRadius: 10, alignContent: 'space-around', marginBottom: 10, marginTop: 0
      }} onPress={() => setIsVisible(!isVisible)}>
      <Text style={{fontSize: 20, color: '#59af9a', alignSelf: 'flex-start', margin: 20,fontWeight:'bold'}}>{item.title}</Text>
      <Image source={{uri:item.photo}}
        style={{width: 25, height: 25,marginEnd:10, alignSelf:'center',justifyContent:'flex-end'}} />    
    </TouchableOpacity>
  );

  const renderItem2 = ({ item }) => (
    <TouchableOpacity style={{borderColor: '#59af9a', height: 35, borderWidth: 1.5, color: 'black', alignSelf: 'flex-start', borderRadius: 5, marginLeft: 5, marginRight: 0, marginBottom: 0, marginTop: 5, justifyContent: 'space-evenly'}}>      
      <Text style={{fontSize: 15, color: 'black', textAlign: 'center', marginLeft: 15, marginRight: 15}}>{item.title}</Text>
    </TouchableOpacity>    
  );
  
  const renderItem3 = ({ item, index },sectionHead) => {
    return (
      <View style={{width:'100%'}}>

              <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginTop: 0, marginBottom: 0}}>{item.title}</Text>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
                  <TextInput style={{flex:1, color: 'black'}} />
                  <Image source={require('../../Assets/icons/expand-plus-icon-green.png')}  
                    style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
                </View>  

                <FlatList 
                    data={item.options}
                    scrollEnabled={true}
                    numColumns={7}
                    listKey={(item, index) => 'D' + index.toString()}
                    columnWrapperStyle={styles.wrapView}       
                    style={{width: '95%', margin: 5, marginTop: 5}}
                    renderItem={(childData) => renderItemInsideOptions(childData, index,sectionHead)} />
      
      </View>    
    );
  };

  const handlleChange = (index,questionIndex,sectionHead) => {
    activityAnswer[sectionHead][0].questions[questionIndex].options[index].isSelected=true   
  };

  const handlleChangeUncheck = (index,questionIndex,sectionHead) => {
    activityAnswer[sectionHead][0].questions[questionIndex].options[index].isSelected=false 
  };


  const renderItemInsideOptions = ({ item ,index},questionIndex,sectionHead) => {
    const { title, isSelected } = item;
    return (
      <TouchableOpacity
      onPress={() => {
        if (isSelected) {
          setSelectedData((prev) => prev.filter((i) => i !== title));
          handlleChangeUncheck(index,questionIndex,sectionHead) 
        } else {
          setSelectedData(prev => [...prev, title])
          handlleChange(index,questionIndex,sectionHead)
        }
      }}
      style={[
        {
          borderColor: '#59af9a', height: 35, borderWidth: 1.5,
           color: 'black', alignSelf: 'flex-start', borderRadius: 5,
            marginLeft: 5,
       marginRight: 0, marginBottom: 0, marginTop: 5,backgroundColor:'white',
        justifyContent: 'space-evenly'}, 
      item.isSelected && {  backgroundColor:item.isSelected? '#59af9a': 'white'}]}>
      <Text style={{fontSize: 15, textAlign: 'center', marginLeft: 15, marginRight: 15, color: isSelected ? "white" : "black"}}>{item.title}</Text>
    </TouchableOpacity>  
    );
  };

  return (
    <View style={{flex: 1}}>
      {!isVisible ?
        (
          <View style={{flex: 1, marginBottom: 20}}>
              <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: demographyFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: healthhistoryFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: foodMoodFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: activityFlag ? '#59af9a' : 'white',marginStart:5}} />
            </View>
            <View style={{backgroundColor: '#D3D3D3', width: '90%', color: 'black', alignSelf: 'flex-start', padding: 5, borderRadius: 10, borderTopLeftRadius: 0, margin: 20, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 18, color: 'black', textAlign: 'auto', marginLeft: 5,padding:5}}>{StringsOfLanguages.Thirty}</Text>
            </View>
            <FlatList 
              data={DATA1}
              scrollEnabled={true}
              renderItem={renderItem1} />
            <View style={{alignSelf: 'center', margin: 20, marginBottom: 5, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 15, color: '#7e7e7e', textAlign: 'center'}}>{StringsOfLanguages.TwentySeven}.</Text>
            </View>
            <TouchableOpacity style={{borderColor: '#e7973d', width: '90%', height: 50, alignSelf: 'center', borderWidth: 2,
              marginBottom: 0, margin: 15, marginTop: 0, borderRadius: 10, justifyContent: 'center'}} >
              <Text style={{textAlign: 'center', fontSize: 20, color: '#e7973d',fontWeight:'bold'}}>{StringsOfLanguages.Nineteen}</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      {isVisible ?
        (
          <ScrollView>
            <View style={{flex:1,marginTop:10,marginStart:10,marginEnd:10,marginBottom:8}}>
            <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            </View>
              <Text style={styles.text1}>{foodMoodTitle}</Text>
              <View>
                
              <FlatList 
                    data={questionsData1}
                    scrollEnabled={false}
                    renderItem={(childData) => renderItem3(childData, 'Eating_Habits')}
                    />

                <FlatList 
                    data={questionsData2}
                    scrollEnabled={false}
                    renderItem={(childData) => renderItem3(childData, 'Preference')}
                    />

              <FlatList 
                    data={questionsData3}
                    scrollEnabled={false}
                    renderItem={(childData) => renderItem3(childData, 'Cooking_Eating_Out')}
                     />

              <FlatList 
                    data={questionsData4}
                    scrollEnabled={false}
                    renderItem={(childData) => renderItem3(childData, 'Cravings_and_stress')} />

        <TouchableOpacity onPress={()=> {
                   axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_foodmood',{
                    "user_id" : userId,
                    "foodmood" :activityAnswer
                  },
                  {
                   "headers": {
                   'Authorization':authToken
                  ,'Content-Type': 'application/json',
                  }
                  })
                  .then((response) => {
                    ToastAndroid.show(response.data.msg,ToastAndroid.SHORT);
                  })
                  .catch((error) => {
                   console.log("ERROR:::  " + error.message);
                }); 
                }} style={{backgroundColor:'#59af9a',width:'95%',height:50,alignSelf: 'center',
                  marginBottom:15, margin:15, borderRadius: 10, justifyContent:'center'}} >
                  <Text style={{textAlign:'center',fontSize:22,color:'white'}}>{StringsOfLanguages.FourttyTwo}</Text>
                </TouchableOpacity>
           
              </View>
            </View>
          </ScrollView>
        ) : null}
    </View>
  )
}
export default FoodMoodpage;

const styles = StyleSheet.create({
  text1:{
    alignSelf:'flex-start',
    fontSize:20,
    paddingLeft: 15, paddingRight: 15,
    color:'#000000',marginBottom:15
  }, 
  text2:{
    fontSize:20,
    color:'#000000',marginTop:10,marginStart:13
  },
  input: {
    height: 60,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:3,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  itenText:{
    fontSize:18,color:'#59af9a',
    fontWeight:'bold',paddingStart:15
  },
  inputEthnivity: {
    height: 120,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:5,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  wrapView: {
    flexWrap: 'wrap',
  },
});