import React, { useState, useEffect } from "react";
import { Text, View, StyleSheet, FlatList, TextInput, Image, TouchableOpacity, ScrollView, LogBox,ToastAndroid } from 'react-native';
import StringsOfLanguages from '../../Constants/StringsOfLanguages';
import AsyncStorage from '@react-native-community/async-storage';
import axios from 'axios';

let healthHistoryAnswer={};

const DATA1 = [
  {
    id: '1',
    title: [StringsOfLanguages.ThirtyFive],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADUAAABBCAMAAABGixyVAAACT1BMVEUAAAAA//+AgIBVqqpAv4BmmZlVqqpJtpJgn59Vqo5Ns5lduaJVqpVbtpJVqplgr59atJZVqpxerpRZs5lVqp5drpdVqpVcrZlbrZtYsJ5arZxYr5ddsptarZ5Xr5lcsZxarJhXrppcsZ1XrptbsJhZsppXrpxXrphar5pYsZxXrZlar5tYsZhXrZpYsJlbrZtZrphYsJpbrZxZrplYsJtZrppasZlZrptYr5hasJpZrptXr5lasJpZrphasJtYrZlbrppYsZparptZr5larptZr5pYsJtZr5tYsJlarppZr5tYr5pasJtYrppar5tZsJlYrppar5tZsJlar5lZsJpYrptZr5tZrppZr5lYsJpZrptasJpZr5pZrplZrppYr5tar5pZsJtYrplZr5pZsJtZr5pZsJlZr5tZr5parppZr5lZr5pasJtZr5pYr5pYr5tZrppYr5lZsJpZrptYr5pZr5pZrplYr5pZr5tar5pZr5lZsJpZr5pZsJpar5lZr5pZsJtZrppYr5lZr5pYr5pZrppZr5lYr5pZr5pZr5par5pZr5par5tZrppZr5pZr5pYsJpZr5pYsJpZr5pZr5pYr5pZr5tZr5par5pZr5lZr5pZrppZr5pZr5lZrppZr5pZr5lZr5pZr5pYr5pZr5pYr5pZr5par5pZr5lZr5pZrppZr5pZsJtZr5pZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5par5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r////wXnrAAAAAw3RSTlMAAQIDBAUGBwgJCgsMDg8QERITFBUWGBkcHR8gISIjJCUmJykqKywvMDEyMzQ1Nzg5Ojs8PT9BQkNERUZHSEpLTE5PUFJWV1laW1xdXmJjZGVmZ2lqa3Byc3R1d3l7fn+DhIWGh4mKjI2Oj5CRkpOWmJmam5ydnp+goqOkpqeoqaqrra+wsbKztbi5ury+v8LExcfIycrLzM3O0NHS09TV2Nna29ze4OHi4+Tm5+jp6uvs7e7v8PHz9PX29/j5+vv8/f5YA5F/AAAAAWJLR0TEFAwb4QAAAy1JREFUGBmVwYlDU3UAB/DvGCpMsXkXQVl0YJJkFK8Ea2hWJplHpFRigRdF2DokrdQkjxQsKpsXvsRjaIUOVEKb4+37j/Wu39jbfrztfT5w8UZDKbw7Qt7qhld7Sd5GDkEFTp0koxCCf+xumAGn8k29ieSLcPiYZD+ELpLxnoeR5qk4dZcCEB7/TPG/T/IX2JbTcLMUaXwHaGiHUE3eOEPyGCwlg9Rp9XAo+pU6rQa2ovs0fQdLmIatyDB/kLo/i2A7R9PnMNUmqevxI1PlKHVtsO2haTsMgcvUDc5BtnXUJRbD0kR+enCMW2DooOH060VwmrO+Z5yGc1NhqiaXILDyMeiqNFpGOnxI4xumsBym4gTLYFvUO07LITg03KUh9uUyHyxnGUDK/KZT1HUVwmnpMMltU5Dy9SjSfU+yzYdMFVEyOh0p715CmmVkshkSD14gW5FS+h4mTBvgf6sg9cDPjFdAaitH6zCJ4m6e9MG2oLw8CNuj9/6qxKT8Ya6Bbt7G3jvU/dPdWALd3otlcLOlBZi5M86UkZbpwMIgcqkdosPFKuTWGGeGu3XIJaQxy70lmNyTzW3vVI1QYqDmw9a3ApAo6NTo6u8XkK2DuYw9h0yhJHOKLYTTQzHm4fJcpCvsY15+8iHNduZpPSbUaczTaBmEwBXm7RCEdnrwMixPjNOD330wHaUnIRiWMiW6j5PYl6QQgeFHConKKsrdnrGbKbUAHtEodACnKfUJZsUoHAbQQuFGEKinzLWZwEYK92cD5ylsgO4rZku+AsB/nsJaBJO0XZ8K3bTDzNIMw9sUvsFLFNphmrKfTvENMBUP03YBb1LYDNvqIab5rRK2/bTFsI7CtfAXB4+dikSOPzPrgwFakn0rClaejET6jn8b/iFKm4YQs2gHni+oaGzd0bkpNM//WoTZMFujxNU9Ta/WVNev3XGFEhrQS8+OAmvoVeJpIHCVHrVCt2iMnpwohOHZ6/SgqxiW4K5/maf+BkwoWR3uG6K78f7uj6qRSaE7FTIK3amQUehOhYxCdypkFLpTIaPQnQoZhe5UyCh0p0JGoTsVMgrdqZBR6E5Fyv+q7sFA4U85sAAAAABJRU5ErkJggg==',
    link: 'AchesScreen',
  },{
    id: '2',
    title: [StringsOfLanguages.ThirtySix],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAA+CAMAAABa1mjFAAAB11BMVEUAAAAA//+AgIBAv4BmmZlVqqpJtpJgn59Vqo5Ns5lduaJVqpVbtpJVqplgr59Vqp5drpdVqpVYsZ1YsJ5Vs5ldsptarZ5Xr5lcsZxZrJlXrptZsppXrpxbsJlZsZtar5tXrZpbrZtZrphYsJpYsJtasZhasZlasJpZrphXr5pasJtYrZlbrppYsZparptZr5pYsJtZr5tYsJlasJtar5par5tZsJlar5tZsJpar5lar5pZr5tYsJlYsJpZr5lasJpZrplZr5pZrppYr5tar5lZsJpar5pZsJtZsJlarppZr5tZr5pZr5pasJtZr5pYr5pasJlYr5tZrppZrptYr5pZrplZr5tar5pZsJpZr5pZsJtZrppZr5pZrppZr5pZrppar5pZr5par5pZr5par5tZr5pZrppZr5pZr5pZrptZr5pYsJpZr5pYsJpZr5pZr5pYr5pZr5tar5pZr5lZr5pZr5pZr5pZr5lZr5pZr5pZsJpZr5lZr5pZr5pYr5pZr5lZr5pZr5par5pZr5lZr5pZrppZr5pZr5pZsJtZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r///8mNvWUAAAAm3RSTlMAAQIEBQYHCAkKCwwODxAVFhgaHR4hIiMkKCkrLC0uMzU4OTo9PkFESElKS0xOT1ZXWVpeYGNkZmpsb3BxdHZ3eHl+f4CBg4eKi4yNkJGSk5SWmJucnqCipKaqq6yur7G2uLm6vL2+v8DBwsTFx8jJysvNzs/Q0tPV1tfY2drb3d/g4eLj5OXm5+jp6uzt7vDx9Pb3+Pn6+/z9/n2Qsh4AAAABYktHRJxxvMInAAACHklEQVQYGY3BCSNUURgG4HfGzFAILaI9RRRRaRktSlRaKG2KtEsqabOlTSgt9pnJvH+2mcNc95x7637Pg6T8us73/R2HMyETaIxQmaiERPABU+InIHCFy+Ll8LRlgTYfA/BymZpSeBmi5gK8zFDTBqCgaTgy8yIcgqtJam4DR6JUenLg5g01Z7GfKd0BuDhDTcmKH7Q0w8W6CG3e+SpoUw0XtVwWLcJx2sxtg5O/hSnRfUAN7b7mwkUnF/XtALCLmq4AnCaYFNvrR0JglJomOPVQaYZykroDcCilEi9Dgv8edXNb4XCdyvc8AKdoGsmBKXuUyrMgtsfo8CQNptI4lZbQAF1chMNVLnpOV9UwZY3wfyI7YSpZoN18PzXjq2FqpN3BvHFqXmfAEHrLZeeBogg1N2HaMMuUh2kAwtQdhSnMJcOrkHSNmj9lMPg6qEwVQkl/Rc2vfBiyB5gQLcOStT+p6cuEIavu6ef7xbCUxKi544OHY9SdhpdWauK7YVPY9uGTaYy6bythyZ2gQBiWGko8hqWVEmOw9FLiCyxTlOhASh5F9iClmBK3YDlEgdZ0WM5RmZ/8p9+PymFzl0o9pIaoVEIoLUJlI4QKqESDEKqgMgipWiptkLpBpQFSvVQqIeSbprIJQuupxEIQqqIyCKlGKu2Q6qLSACH/NJUqCK3hos0QSp9lUiwIqUtM+gixrJdMaIdcRv1IrDsf3v4C/hPlh+nEOz4AAAAASUVORK5CYII=',
    link: 'EnergyScreen',
  },{
    id: '3',
    title: [StringsOfLanguages.ThirtySeven],
    photo: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAC8AAAA/CAMAAABHL2EFAAACNFBMVEUAAAAA//9VqqpAv4BmmZlVqqpJtpJgn59Vqo5Ns5lVqpVisZ1btpJVqplgr59VqpxerpRZs5ldrpdZsZtVqpVcrZlYsZ1brZtYsJ5arZxYr5ddsptarZ5arJhcsZ1XrpxbsJlZsZtXrphYsZxXrZlYsZhXrZpar5xYsJlbrZtZrphYsJpZrplasZlZrptasJpZrptXr5lXr5pasJtYrZlbrppZsJhYsZparptZr5larptYsJtarppZr5tYsJlarppYr5pasJtZsJtZsJlYrppar5tZsJlYrppZsJpYrptar5lZr5pYrplar5pZr5tYsJlZrppZr5lYsJpZr5lasJpZrplZr5pasJtYr5par5lZrppYr5tar5lar5pZsJtYrplZr5pYrppZr5pZsJlZr5parppZr5pasJtYr5pasJlZr5pYr5tasJpZsJpZrptZr5pZrplYr5pZr5tar5pZsJpar5tZr5par5lZsJtZr5pZrppZr5pYr5pZrppZr5lYr5pZr5pZr5pZr5pZr5pZr5pZr5pZrptZr5pYsJpZr5pZr5tZr5pYr5pZr5pZr5pZr5pZrppZr5pZr5lZrppZsJpZr5lZr5pZr5pYr5pZr5pZr5lYr5pZr5pZr5pZr5lZrppZr5pZr5pZsJtZr5pZr5pZr5pZr5pZr5tZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5par5pZr5pZsJpZr5pZr5pZr5pZr5pZr5pZr5pZr5pZr5r///+jTtx0AAAAunRSTlMAAQMEBQYHCAkKDA0ODxASExQWFxgZGhwdHyAhIiUnLC0uLzEyNDU2Nzg5OjxBQkRFRklKS0xNTk9QUlRYWVpbXV5hZGVmZ2hqa2xtbm9wcXJzdHZ3eHl6fH1+f4CDhIWGiImKjY6QkZOUlZaXmpudnp+goqSlpqiqrK6vsLGys7W6u72/wMHDxMXGyMrMz9DR0tPU19jZ2tvc3d7f4OLk5ebn6Onq6+zt7u/w8fLz9PX29/j5+vv8/f6P95W6AAAAAWJLR0S71LZ3TAAAAqNJREFUGBmVwQdDTWEABuC3o8hKhZC9d66ZUcjmZmfTQBllr7KLirhGImRHpaJwW++vc7/vfJ3OyT3D86C36L74D5NvB9lYujMG3vg7KDWlwYsUGrbA3cB6Gv6Mg6sNNMmDq6s0qYSr+zRpgKubNHkPV4doUgRX4zrYYxnc5dNQFgF3fe9RqRoOLyKzWhnSeX4IPBqy8kjOhhH4X4Pm557eGAOPBpRQ+JYMb7LImpOXv7A9GV5E1HYc2FVZMn0z6+LhQVzrvhKSH/s3MhseDD7YSGFiDes0uBr/kFLzmA5yDlwMy2uj1JJyl2Q2HCXk/qLudUoVQ57CXsTcwjYqZzNaKHQlwMakw1Xs9mpbBZUlCCN20fG3NPzYfzTIbnvRS9S6S9U0aTkah280XIFVn2Ka1WYNBVBGwxNYraVJWUoUhAIa3sHqJJX2e1tHQ9lOQy2sJvwk+fvRqd0ZN74X+xMhpdLQjF5G+denbT73grpnmdMjgCQagpAK3wqPkxAygxafzyyZSkMDpCClDwiZyd6CNFRD0KggJJEOKiBoVBDSjw7uQNCoQGiivQIIGhUIL2lvOQSNCoRSSp3816doCBoVCNco1ccvPfbgN826UiFpVCCcoNTVD0DUrB3Xv1D5swk6jQqEvdQlQhmzOr88UFqUkQAlkgqENdTNhp1VVCAsoG4FMHJeGGm32A3CFOr8QDqdQUigLhtIpzMIfTopXQQ20hmkr5SKAR+dQXpOqRKIfENHkIopNQBY2EYnkC5Q+omQ5Do6gLSGUjmE2P0vaQtS9DMKC6HEN9MGdMOutpJ7YCihDXTTcuajRyZtYGyOlR/CYtqAj1YBCLFdDA8+WgUgvWF48NEqAOkyw4OPVgFIfoYHH60CkKYxPPhoFYAU+Yth/QV5uCyKSahf9AAAAABJRU5ErkJggg==',
    link: 'BowelScreen',
  }
]

const HealthHistory = () => {

  const [isVisible, setIsVisible] = useState(false);
  const [selectedData, setSelectedData] = React.useState([]);

  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');
  const [isLoading, setLoading] = useState(true);

  const [achesAndPaintitle, setAchesAndPaintitle] = useState('');
  const [questionObject,setQuestions]=React.useState([]);

  const [energyStressLeveltitle, setenergyStressLeveltitle] = useState('');
  const [questionObject1,setQuestions1]=React.useState([]);

  const [bowlMovementtitle, setbowlMovementtitle] = useState('');
  const [questionObject2,setQuestions2]=React.useState([]);
  
  const [demographyFlag, setDemographyFlag] = useState(false);
  const [healthhistoryFlag, setHealthhistoryFlag] = useState(false);
  const [foodMoodFlag, setfoodMoodFlag] = useState(false);
  const [activityFlag, setActivityFlag] = useState(false);
 

  useEffect(() => {
    AsyncStorage.getItem('authToken').then(
      (value) =>{
      setAuthToken(value);
      AsyncStorage.getItem('UserId').then(
        (value1) =>{
          setUserID(value1) 
          axios(`http://18.139.75.180:3000/sunev/api/v1/user/user_questionnaire/${value1}`,{
            "headers": {
            'Authorization':value
           }})
            .then((response) => {   
            
              setDemographyFlag(response.data.data.is_demography_complete)
              setHealthhistoryFlag(response.data.data.is_healthhistory_complete)
              setfoodMoodFlag(response.data.data.is_foodmood_complete)
              setActivityFlag(response.data.data.is_activity_complete)

              let healthhistoryObject = response.data.data.healthhistory.Aches_and_pains
              setAchesAndPaintitle(healthhistoryObject[0].section_title)
              setQuestions(healthhistoryObject[0].questions)
    
              let healthhistoryObject1 = response.data.data.healthhistory.Energy_and_Stress_Levels
              setenergyStressLeveltitle(healthhistoryObject1[0].section_title)
              setQuestions1(healthhistoryObject1[0].questions)
    
              let healthhistoryObject2 = response.data.data.healthhistory.Bowel_Movement
              setbowlMovementtitle(healthhistoryObject2[0].section_title)
              setQuestions2(healthhistoryObject2[0].questions)
    
              healthHistoryAnswer=response.data.data.healthhistory
              
            })
            .catch((error) => console.error(error))
            .finally(() => setLoading(false));
          }
        );
        }
      );
     
    LogBox.ignoreLogs(["VirtualizedLists should never be nested"])
  }, [])

  const renderItem1 = ({ item }) => (
    <TouchableOpacity style={{margin: 20, flexDirection: 'row',justifyContent:'space-between',paddingEnd:10, borderColor: '#59af9a', borderWidth: 2, borderRadius: 10, alignContent: 'space-around', marginBottom: 10, marginTop: 0
      }} onPress={() => setIsVisible(!isVisible)}>
      <Text style={{fontSize: 20, color: '#59af9a', alignSelf: 'flex-start', margin: 20,fontWeight:'bold'}}>{item.title}</Text>
      <Image source={{uri:item.photo}}
        style={{width: 25, height: 25,marginEnd:10, alignSelf:'center',justifyContent:'flex-end'}} />    
    </TouchableOpacity>
  );

  const renderItemStarRating= ({ item, index }) => {
    return(
      <View>
      <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5}}>{item.title}</Text>
      <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
        <TextInput style={{flex:1, color: 'black'}} />
        <Image  source={require('../../Assets/icons/expand-plus-icon-green.png')} 
          style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
      </View> 
      </View>
      
    );
  }

  const renderItem2 = ({ item, index }) => {
    return (
      <View   style={{width: '100%'}}    >
               <Text style={{fontSize: 15, color: '#59af9a', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5}}>{item.title}</Text>
                  <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', backgroundColor: '#FFF5EE', borderWidth: 2, borderColor: '#ccc', height: 60, borderRadius: 10, marginTop: 10, margin: 10, marginBottom: 0}}>
                    <TextInput style={{flex:1, color: 'black'}} />
                    <Image source={require('../../Assets/icons/expand-plus-icon-green.png')} 
                    style={{padding: 10, margin: 15, height: 25, width: 25, resizeMode : 'stretch', alignItems: 'center'}} />
                  </View>  

                <FlatList 
                    data={item.options}
                    scrollEnabled={true}
                    numColumns={7}
                    listKey={(item2, index) => 'E' + index.toString()}
                    columnWrapperStyle={styles.wrapView}       
                    style={{width: '95%', margin: 5, marginTop: 5}}
                    renderItem={(childData) => renderItemInsideOptionsRadio(childData, index)}
                     />
      
      </View>    
    );
  };


  const renderItemInsideOptionsRadio = ({ item,index },questionIndex) => {
    var { title, isSelected } = item;
    return (
      <TouchableOpacity onPress={() => {
        isSelected=!isSelected
        console.log(isSelected)
        for(let i = 0 ; i < questionObject[questionIndex].options ; i++ ){
          questionObject[questionIndex].options[i].isSelected = false;
        }
        questionObject[questionIndex].options[index].isSelected = true;
        setQuestions(questionObject);

        console.log("item 3", questionObject[questionIndex].options);
        item.isSelected = true;
      }}

     
      style={[{borderColor: '#59af9a',backgroundColor:'white', height: 35, borderWidth: 1.5, color: 'black', alignSelf: 'flex-start', borderRadius: 5, marginLeft: 5, marginRight: 0, marginBottom: 0, marginTop: 5, justifyContent: 'space-evenly'}, 
        { backgroundColor: isSelected ?'#59af9a':'white'}]}>     
       
      <Text style={{fontSize: 15, color: 'black', textAlign: 'center', marginLeft: 15, marginRight: 15}}>{item.title}</Text>
    </TouchableOpacity>    
    );
  };
  
  return (
    <View style={{flex: 1}}>
      {!isVisible ?
        (
          <View style={{flex: 1, marginBottom: 20}}>

            <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: demographyFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: healthhistoryFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: foodMoodFlag ? '#59af9a' : 'white',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: activityFlag ? '#59af9a' : 'white',marginStart:5}} />
            </View>

            <View style={{backgroundColor: '#D3D3D3', color: 'black', alignSelf: 'flex-start', padding: 5, borderRadius: 10, borderTopLeftRadius: 0, margin: 20, marginBottom: 0, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 18, color: 'black', textAlign: 'left', marginLeft: 5,padding:5}}>{StringsOfLanguages.Twentyeight}</Text>
            </View>
            <View style={{backgroundColor: '#D3D3D3', width: '90%', color: 'black', alignSelf: 'flex-start', padding: 5, borderRadius: 10, borderTopLeftRadius: 0, margin: 20, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 18, color: 'black', textAlign: 'auto', marginLeft: 5,padding:5}}>{StringsOfLanguages.Twentynine}</Text>
            </View>
            <FlatList 
              data={DATA1}
              scrollEnabled={true}
              renderItem={renderItem1} />
            <View style={{alignSelf: 'center', margin: 20, marginBottom: 5, marginTop: 10, justifyContent: 'space-evenly'}}>
              <Text style={{fontSize: 15, color: '#7e7e7e', textAlign: 'center'}}>{StringsOfLanguages.TwentySeven}</Text>
            </View>
            <TouchableOpacity style={{borderColor: '#e7973d', width: '90%', height: 50, alignSelf: 'center', borderWidth: 2,
              marginBottom: 0, margin: 15, marginTop: 0, borderRadius: 10, justifyContent: 'center'}} >
              <Text style={{textAlign: 'center', fontSize: 20, color: '#e7973d',fontWeight:'bold'}}>{StringsOfLanguages.Nineteen}</Text>
              </TouchableOpacity>
          </View>
        ) : null}        
      {isVisible ?
        (
          <ScrollView>
            <View style={{flex:1,marginTop:10,marginStart:10,marginEnd:10,marginBottom:8}}>
            <View style={{flexDirection:'row',margin:5}}>
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            <View style={{ height: 3,marginTop:5,flex:1, backgroundColor: '#59af9a',marginStart:5}} />
            </View>
            
              <Text style={styles.text1}>{achesAndPaintitle}</Text>

              <View>
               
                  <FlatList 
                    data={questionObject}
                    scrollEnabled={false}
                    renderItem={renderItem2} 
                    />
                
                <Text style={{fontSize: 20, color: 'black', textAlign: 'left', margin: 15, marginBottom: 0, marginTop: 5}}>{energyStressLeveltitle}</Text>
                <View >

                <FlatList 
                    data={questionObject}
                    scrollEnabled={false}
                    renderItem={renderItemStarRating} 
                    />
                </View> 
              
              
                <Text style={{fontSize: 18, color: 'black', textAlign: 'left', margin: 15, marginTop: 5, marginBottom: 0}}>{bowlMovementtitle}</Text>

                <FlatList 
                    data={questionObject2}
                    scrollEnabled={false}
                    renderItem={renderItem2} />


                <TouchableOpacity onPress={()=> {
                   axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_healthhistory',{
                    "user_id" : userId,
                    "healthhistory" :healthHistoryAnswer
                  },
                  {
                   "headers": {
                   'Authorization':authToken
                  ,'Content-Type': 'application/json',
                  }
                  })
                  .then((response) => {
                    ToastAndroid.show(
                      response.data.msg,
                      ToastAndroid.SHORT
                    );
                  })
                  .catch((error) => {
                   console.log("ERROR:::  " + error.message);
                }); 
                }} style={{backgroundColor:'#59af9a',width:'95%',height:50,alignSelf: 'center',
                  marginBottom:15, margin:15, borderRadius: 10, justifyContent:'center'}}>
                  <Text style={{textAlign:'center',fontSize:22,color:'white'}}>{StringsOfLanguages.FourttyTwo}</Text>
                </TouchableOpacity>
              </View>
            </View>
          </ScrollView>
        ) : null}
    </View>
  )
}
export default HealthHistory;

const styles = StyleSheet.create({
  text1:{
    alignSelf:'flex-start',
    fontSize:20,
    paddingLeft: 15, paddingRight: 15,
    color:'#000000',marginBottom:15
  }, 
  text2:{
    fontSize:20,
    color:'#000000',marginTop:10,marginStart:13
  },
  input: {
    height: 60,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:3,
    fontSize: 16,marginStart:10,marginEnd:10
  },
  itenText:{
    fontSize:18,color:'#59af9a',
    fontWeight:'bold',paddingStart:15
  },
  inputEthnivity: {
    height: 120,
    backgroundColor: '#fff',
    borderColor: '#ccc',
    borderWidth: 1,
    borderRadius: 10, marginTop:5,
    fontSize: 16,marginStart:10,marginEnd:10
  },
 });