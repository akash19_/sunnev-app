import React from 'react';
import { Text, View,StyleSheet ,Image,ImageBackground,FlatList} from 'react-native';
import { Appbar } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';

const DATA = [
  {
    id: '1',
    title: 'Meal Plan',
    backgroundColor:'#dd722e'
  }, {
    id: '2',
    title: 'Food tracking',
    backgroundColor:'#57a5d7'
  },
  {
    id: '3',
    title: 'grocery list',
    backgroundColor:'#b57cc3'
  },{
    id: '4',
    title: 'Receipes',
    backgroundColor:'#a0b95b'
  }, 
  {
    id: '5',
    title: 'Activity',
    backgroundColor:'#59af9a'
  }, {
    id: '6',
    title: 'Nutrition guidance',
    backgroundColor:'#dd722e'
  }, {
    id: '7',
    title: 'Community Resources',
    backgroundColor:'#57a5d7'
  }, {
    id: '8',
    title: 'Topics',
    backgroundColor:'#b57cc3'
  }
]


const Dashboardpage = () => {
  const navigation = useNavigation(); 

  const renderItem = ({ item, index }) => {
    return (
      <View  style={[styles.carrdStyle, { backgroundColor: item.backgroundColor }]}>
      <Image  resizeMode='contain' source={require('../../Assets/icons/health-polatter-icon.png')} style={{width:80,height:80,alignSelf:'center'}} />
      <Text style={{alignSelf:'center',fontSize:16,fontWeight:'bold',color:'white'}}>{item.title}</Text>
      </View>
    );
  };


  return (
    <View>
      <Appbar.Header style={styles.toolButton}>
        <Appbar.Action size={35} icon="chevron-left"   onPress={() => navigation.navigate('Onboardingend')} />
        <Appbar.Content title="" subtitle="" />
       
        <ImageBackground
          style={{width: 25, height: 25}}
          source={{uri:'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACEBAMAAACjap6UAAAAIVBMVEVHcEx1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXV1dXUW5UrxAAAACnRSTlMAQNHssG8RV5QoA+O9TAAAAxtJREFUaN7tWEtv00AQTtLUTjgVOETKKbTi5RO0AlGfcqLgUwQCqpyiAuJxSlBBglMFFImcKopaxKmxHSj7KylkvV7bs/aMO0Ic8p2sXe+3u/PamalU8nD4ccUNVu+8rZSF/UFI3BqWY/jmCoXgcxmGryKBu3SG5yKFT2Q5uGmKgCgPyxMZhAMSxRcB4CqFoepCFMESgeK8AHH9tIcgHWNfGLCOVodjovCxSqlra3ZHrU2NsYukeKdWbPzd1dpWAz+R93AzfqH8JcDdpAHoUGl5B0XxHTBoZfC/UBQetGF0tBDD0JQ/HyeHx3J4jyCKLqxpjDAWYNlHerqGt4pJeryNt4yxwQ7roIhAuAaxSTEHaIWERmUXq8Q2XlkKaYjV6Y/szEusVqXUOtmZGtbfa8a9GkZy2LJ62ZlFrG0tGIVmYynOzH4EYnV1NnP0LygYLsIgTgalMpgWg4EzuBmDszOEHI7AxxB+GR4BhqeI4UFkeJY5kgOGFIUhUWJI1ziSRobUlSGB5kjjGYoJjpKGobDiKO8YikyOUpej4OYo+zmaDxwtkDn+G7xovX4A4NHoALe+ubkijFjdLX4QrW1X5CLYKPB52xOFCHOt/akrEAge5sQIFMMJhzF2LCIZTjh6hrDtCDR8MJhbY0HAMaSXc4KEC8ZSAY+eKWMlXCXN8ESQcTs/pVhbBrCWm2xs6XNXRgY3sFrv9f8uJSa1Q4Rv8lzgmWd4ZLXs6mKBI1r34MwrVsfl4oDyClKKrQZvYILSffX7MJue4VKIOPlYzwqzh4uNypL9zAg6HdtP7xml/v4elqLppAoDDzSVXGwly5MqOTvWjrGk1+KE7FiTRidRO5AS02qiunDwNWzWoH2dr0OjqGlnr5e5R7xzN25LEO+hbvKn0dGffd6kUjyerZvG0tyhUjSUPC15pwGVIl5oUwppqDAfRgqZ0CnakUpq+LYE3Ojo4FtN5vZVG9tqMrWvJpFZDOgUVmQY42QQpMCRZu2V1WlFLXXKeUjsJb5spk3LUPRlu43W2oBaJXwU07Ml0E9QnAZzijnFnCIfvwHnd+SGyMAN3wAAAABJRU5ErkJggg=='}} >
          <Image source={require('../../Assets/icons/reddot.png')} style={{width: 8, height: 8, alignSelf: 'flex-end', marginTop: -5, marginRight: -5, position: 'absolute'}} />
        </ImageBackground>

       <Appbar.Action size={35} icon="menu" />
      </Appbar.Header>

      <View style={{backgroundColor:'#59af9a',height:120,marginStart:15,marginEnd:15,borderRadius:10,marginTop:15,flexDirection:'column'}}>
      <Image  resizeMode='contain' source={require('../../Assets/icons/health-polatter-icon.png')} style={{width:80,height:80,alignSelf:'center'}} />
      <Text style={{alignSelf:'center',fontSize:20,fontWeight:'bold',color:'white'}}>Health Platter</Text>
      </View>

      <FlatList 
          data={DATA}
          scrollEnabled={true} 
          numColumns={2}
          renderItem={renderItem}/>

    </View>
  )
}
export default Dashboardpage;


const styles = StyleSheet.create({  
  toolButton: {
    backgroundColor: 'white',  
    borderColor: 'white',
  },carrdStyle:{
    flex:1, 
     justifyContent: 'center', 
      alignItems: 'center',height: 120, 
      margin: 5,marginStart:10,
      marginEnd:10,borderRadius:10}
});  