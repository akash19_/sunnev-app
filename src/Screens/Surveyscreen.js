import * as React from 'react';
// import React, { useState,useEffect } from "react";
import { Text, View ,Image, ImageBackground} from 'react-native';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import Demographeyscreen from "../Screens/Survey/Demographeyscreen";
import HealthHistory from "../Screens/Survey/HealthHistory";
import FoodMoodpage from "../Screens/Survey/FoodMoodpage";
import Activitypage from "../Screens/Survey/Activitypage";
import StringsOfLanguages from '../Constants/StringsOfLanguages';

const Tab = createMaterialTopTabNavigator();

export default function App() {

  // useEffect(() => {
  //   LogBox.ignoreLogs(["VirtualizedLists should never be nested"])
  // }, [])

  return (
    <Tab.Navigator 
    tabBarOptions={
      { activeTintColor: 'white',
      inactiveTintColor: 'white',
      indicatorStyle: { backgroundColor: '#dd722e', height: '100%' },
      pressOpacity: 1,}
    }
    screenOptions={{
      component:{Demographeyscreen},
      tabBarLabelStyle:{textTransform:'none',fontSize:10,paddingTop:3},
       tabBarStyle: { backgroundColor: '#e7973d',height:75}, }}>
      <Tab.Screen
        name={StringsOfLanguages.Twentyone}
        component={Demographeyscreen}
        options={{
          tabBarIcon: () => (
            <ImageBackground style={{height:30,width:20}} source={require('../Assets/icons/demography-tab-icon.png')} >
              <Image source={require('../Assets/icons/reddot.png')} style={{width: 8, height: 8, alignSelf: 'flex-end', marginTop: -5, marginRight: -5, position: 'relative'}} />
            </ImageBackground>
          ),
        }}/>
      <Tab.Screen
        name={StringsOfLanguages.Twentytwo}
        component={HealthHistory}
        options={{
          tabBarIcon: () => (
            <ImageBackground style={{height:30,width:20}}  source={require('../Assets/icons/health-history-tab-icon.png')} >
              <Image source={require('../Assets/icons/reddot.png')} style={{width: 8, height: 8, alignSelf: 'flex-end', marginTop: -5, marginRight: -5, position: 'relative'}} />
            </ImageBackground>
            ),
        }}/>
      <Tab.Screen
        name={StringsOfLanguages.Twentythree}
        component={FoodMoodpage}
        options={{
          tabBarIcon: () => (
            <ImageBackground style={{height:30,width:20}}  source={require('../Assets/icons/food-mood-tab-icon.png')} >
              <Image source={require('../Assets/icons/reddot.png')} style={{width: 8, height: 8, alignSelf: 'flex-end', marginTop: -5, marginRight: -5, position: 'relative'}} />
            </ImageBackground>

          ),
        }} />
       <Tab.Screen
        name={StringsOfLanguages.TwentyFour}
        component={Activitypage}
        options={{
          tabBarIcon: () => (
            <ImageBackground style={{height:30,width:20}}  source={require('../Assets/icons/activity-tab-icon.png')} >
              <Image source={require('../Assets/icons/reddot.png')} style={{width: 8, height: 8, alignSelf: 'flex-end', marginTop: -5, marginRight: -5, position: 'relative'}} />
            </ImageBackground>
          ),
        }}/>
      
    </Tab.Navigator>
  );
}


