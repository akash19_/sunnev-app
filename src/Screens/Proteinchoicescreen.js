import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, FlatList,Image,ToastAndroid } from 'react-native';
import { width, height } from 'react-native-dimension';
import { Appbar } from 'react-native-paper';
import StringsOfLanguages from '../Constants/StringsOfLanguages';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';

const Proteinchoicescreen = ({route, navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [authToken, setAuthToken] = useState('');
  const [userId, setUserID] = useState('');


  


  // const [brands, setBrands] = React.useState(DATA);

  useEffect(() => {

    AsyncStorage.getItem('authToken').then(
      (value) =>{
        setAuthToken(value)
        AsyncStorage.getItem('UserId').then(
          (value1) =>{
            setUserID(value1)
            axios(`http://18.139.75.180:3000/sunev/api/v1/user/get_proteins/${value1}`,{
          "headers": {
            'Authorization':value
          }})
          .then((response) => setData(response.data))
          .catch((error) => console.error(error))
          .finally(() => setLoading(false));
      }
    );
          }
          
        );
          
  }, []);


  const [selectedProtein, setSelectedProtein] = React.useState([]);

  
  const renderProtienChoice = ({ item, index }) => {

    const { protein_id, title } = item;
    const isSelected = selectedProtein.filter((i) => i.protein_id === protein_id).length > 0;

    return (
      <View style={{marginStart:20,marginEnd:20,marginTop:2}}>
     <TouchableOpacity onPress={() => {
          if (isSelected) {
            setSelectedProtein((prev) => prev.filter((i) => i.protein_id !== protein_id));
          } else {
            setSelectedProtein(prev => [...prev,{protein_id: protein_id} ])
          } 
        }
      }
        style={[styles.item, isSelected && {borderRadius:10, backgroundColor: '#DCDCDC' }]}>
          
        <View style={{margin:8,  flexDirection:'row'}}>
  <Image style={{width:30,height:30,alignSelf:'center',borderRadius:5}}
    source={isSelected
        ? require('../Assets/icons/button-clicked.png')  
        :require('../Assets/icons/button-not-clicked.png')} />

    <Text style={{fontSize:20,color:'black',alignSelf:'center',marginStart:15}}>{item.title}</Text>
  </View>
      </TouchableOpacity>
      </View>
     
    );
  };

  return (      
    <View style={styles.container}>
      <Appbar.Header style={styles.toolButton}>
        <Appbar.Action size={45} icon="chevron-left"  onPress={() => navigation.navigate('Cusinescreen')}  />
        <Appbar.Content title="" subtitle="" />
        <Appbar.Action size={45} icon="menu"  />
      </Appbar.Header>
      {/* <ProgressBar styleAttr="Horizontal" /> */}
     <Text style={styles.textHeader}>{StringsOfLanguages.twelve}</Text>
     <Text style={styles.textHeader}>{StringsOfLanguages.twelve1}</Text>
     <FlatList 
          data={data.data}
          scrollEnabled={true}
          renderItem={renderProtienChoice}
        />


<TouchableOpacity style={[styles.button,{backgroundColor: '#e7973d'}]}  onPress={() => {
          if(selectedProtein.length>0){
            axios.post('http://18.139.75.180:3000/sunev/api/v1/user/add_protein',{
              "user_id" : userId,
              "preffered_protein" :selectedProtein
            },
            {
             "headers": {
             'Authorization':authToken
            ,'Content-Type': 'application/json',
            }
            })
            .then((response) => {
              ToastAndroid.show(
                response.data.msg,
                ToastAndroid.SHORT
              );
              // console.log(selectedProtein)
              navigation.navigate('Onboardingend')
            })
            .catch((error) => {
             console.log("ERROR:::  " + error.message);
          }); 
          }else if(selectedProtein.length==0){
            alert('Please select protein choices')
          }
          
        }}>
          <Text style={[styles.text2, {color: 'white'}]}>{StringsOfLanguages.eleven1}</Text>
        </TouchableOpacity>
    </View>
  )
}
export default Proteinchoicescreen;

const styles = StyleSheet.create({  
  container: {  
    backgroundColor: 'white',
    flex: 1, 
  },
  toolButton: {
    backgroundColor: 'white', 
    borderColor: 'white',
  },
  logo: { 
    margin: 25, 
    marginTop: 75,
    width: 100, 
    height: 100, 
    alignSelf: 'center', 
  },
  thanks: {
    textAlign: 'center',
    fontSize: 30,
    color: 'black',
    marginBottom: 25,
  },
  content: {
    textAlign: 'center',
    fontSize: 25,
    color: '#097969',
    paddingLeft: 20,
    paddingRight: 20,
  },
  selectBox: {
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute', 
    bottom: 0,
  },
  button:{    
    width: '90%',
    height: 50,
    alignSelf: 'center',
    borderStyle: 'solid', 
    borderColor: '#e7973d',
    borderWidth: 1,
    borderRadius: 10,
    justifyContent: 'center',
    marginBottom: 15, marginTop: 15,
    backgroundColor: '#e7973d',
  },
  text2:{
    fontSize: 22,
    color: '#ff9933',
    textAlign: 'center',
    fontWeight: '500',
    paddingLeft: 25,
    paddingRight: 25,
  },
  textHeader:{
    fontSize: 30,
    color: 'black',
    textAlign: 'center',
    fontWeight: '500',
    paddingLeft: 25,
    paddingRight: 25,
  }
});  