import React from 'react';
import { Text, View, StyleSheet, Image, ImageBackground ,SafeAreaView} from 'react-native';
import StringsOfLanguages from '../Constants/StringsOfLanguages';


const Welcomescreen = ({route, navigation}) => {
  setTimeout(() => {
    navigation.navigate('Reasonselectscreen')
 }, 2000);  
 

  return (
    <SafeAreaView style={styles.container}>
      <ImageBackground
        style={{flex: 1}}  source={require('../Assets/icons/welcomebg.png')} >
          <View style={{marginTop:80}}>
          {/* <ProgressBar styleAttr="Horizontal" /> */}
      <Image  source={require('../Assets/icons/Welcomeicon.png')} resizeMode='contain'
        style = {{ margin: 30, width: 100, height: 100, alignSelf: 'center' }}/>        
      <Text style={styles.welcome}> {StringsOfLanguages.first}</Text>
      <Text style={styles.content}> {StringsOfLanguages.second}</Text>
      <Text style={styles.content1}> {StringsOfLanguages.second1}</Text>
      <Text style={styles.content}>{StringsOfLanguages.third}</Text>
      <Text style={styles.content1}>{StringsOfLanguages.third1}</Text>
    </View>
      </ImageBackground>
    </SafeAreaView>
  
  )
}
export default Welcomescreen;

const styles = StyleSheet.create({  
  welcome: {
    textAlign: 'center',
    fontSize: 30,
    color: '#FF4433',
    marginBottom: 20,marginTop:25
  },
  content: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    paddingLeft: 45,
    paddingRight: 45,
    marginBottom: 0,
  },
  content1: {
    textAlign: 'center',
    fontSize: 20,
    color: 'black',
    paddingLeft: 45,
    paddingRight: 45,
    marginBottom: 10,
  },
  container: {
    flex: 1,
    padding: 10,
  }
});  
