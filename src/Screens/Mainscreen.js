import React, { useEffect, useState } from 'react';
import { Text, View,Image,FlatList, ScrollView ,StyleSheet,TouchableOpacity} from 'react-native';
import StringsOfLanguages from '../Constants/StringsOfLanguages';
import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';



const Mainscreen = ({route, navigation}) => {

  const [selectedService, setSelectedService] = React.useState([]);
  const [data, setData] = useState([]);

  useEffect(() => {
    AsyncStorage.getItem('authToken').then(
      (value) =>{
      AsyncStorage.getItem('UserId').then(
        (value1) =>{
          axios(`http://18.139.75.180:3000/sunev/api/v1/user/get_services/${value1}`,{
            "headers": {
            'Authorization':value
           }})
            .then((response) => setData(response.data.data))
            .catch((error) => console.error(error))
           
          }
        );
        }
      );
  }, []);

 const renderItem = ({ item, index }) => {
  const { id, title } = item;
  const isSelected = selectedService.filter((i) => i === title).length > 0;

  return (
   <TouchableOpacity>
        <View style={[styles.maincontainer,isSelected && {backgroundColor:'gray'}]} >
        <Image source={require('../Assets/icons/test.png')} style={{width:30,height:30,alignSelf:'center'}} />      
    <Text style={{fontSize:18,color:'white',alignSelf:'center',fontWeight:'bold',paddingStart:30}}>{item.title}</Text>
   </View>
    </TouchableOpacity>
  );
};

 

  return (
    <View style={{flex: 1}}>
       <Image source={require('../Assets/icons/Welcomeicon.png')} resizeMode='contain' style={styles.imgLogo} />
      <Text style={styles.textSty}>{StringsOfLanguages.six} </Text>
      <Text style={styles.textSty}> {StringsOfLanguages.seven}</Text>
      <FlatList 
          data={data}
          scrollEnabled={true}
          renderItem={renderItem}/>

<TouchableOpacity style={styles.nextbtn} onPress={() =>
    navigation.navigate('Thankscreen')
  }>
    <Text style={styles.txtNext}>{StringsOfLanguages.eleven1}</Text>
  </TouchableOpacity>
    </View>
  )
}

export default Mainscreen;


const styles = StyleSheet.create({
  maincontainer:{
    padding: 17,
    backgroundColor: '#dd722e', 
    borderColor:'black',
    borderRadius:10,
    margin:5,
    marginStart:20,marginEnd:20,
     borderWidth:0.5,flexDirection:'row'
  },
  imgLogo: {
    width:90,height:90,
    marginTop:30,alignSelf:'center'
 },textSty:{
  fontSize:26,
  color:'black',
  alignSelf:'center',
  marginTop:0,
 },nextbtn:{
  padding: 10, 
  backgroundColor: '#e7973d',
  borderColor:'white',
  borderRadius:10,
  marginStart:20,marginEnd:20,
  marginBottom:10, borderWidth:0.5 
 },txtNext:{
  fontSize:20,color:'white',
  alignSelf:'center',paddingTop:3,
  fontWeight:'bold',marginBottom:10
 }
 });