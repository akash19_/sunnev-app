 import React from 'react';
 import { createNativeStackNavigator } from '@react-navigation/native-stack';
 import Loginscreen from '../Screens/Loginscreen';  
 import Splashscreen from '../Screens/Splashscreen';
 import Reasonselectscreen from '../Screens/Reasonselectscreen'; 
 import Mainscreen from '../Screens/Mainscreen';
import Welcomescreen from '../Screens/Welcomescreen';
import CountrySelectscreen from '../Screens/CountrySelectscreen';
import Thankscreen from '../Screens/Thankscreen';
import Onboardingend from '../Screens/Onboardingend';
import Proteinchoicescreen from '../Screens/Proteinchoicescreen';
import Cusinescreen from '../Screens/Cusinescreen';
import Surveyscreen from '../Screens/Surveyscreen';
import SurveeyHeader from '../Screens/Survey/SurveeyHeader'
import Dashboardpage from '../Screens/DashBoard/Dashboardpage'


 const Stack = createNativeStackNavigator();
 
 const UserNavigationswitch = () => {
   return (
       <Stack.Navigator initialRouteName="Splashscreen"  >
          <Stack.Screen name='Loginscreen' component={Loginscreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Splashscreen' component={Splashscreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Reasonselectscreen' component={Reasonselectscreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Mainscreen' component={Mainscreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Welcomescreen' component={Welcomescreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='CountrySelectscreen' component={CountrySelectscreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Thankscreen' component={Thankscreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Onboardingend' component={Onboardingend} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Proteinchoicescreen' component={Proteinchoicescreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Cusinescreen' component={Cusinescreen} options={{ headerShown: false }} ></Stack.Screen>
          <Stack.Screen name='Surveyscreen' component={Surveyscreen} options={{ header: () => <SurveeyHeader />  }}  ></Stack.Screen>
          <Stack.Screen name='Dashboardpage' component={Dashboardpage} options={{ headerShown: false }} ></Stack.Screen>
       </Stack.Navigator>
   );
 };
 
 export default UserNavigationswitch;


 
 