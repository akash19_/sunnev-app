import React from 'react';
import { SafeAreaView} from 'react-native';

import {
  Colors
} from 'react-native/Libraries/NewAppScreen';
import 'react-native-gesture-handler';
import UserNavigationswitch from './src/Routes/UserNavigationswitch';
import { NavigationContainer } from '@react-navigation/native';
const App = () => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: Colors.backgroudColor }}>
      <NavigationContainer>
        <UserNavigationswitch />
      </NavigationContainer>
    </SafeAreaView>
  );
};


export default App;